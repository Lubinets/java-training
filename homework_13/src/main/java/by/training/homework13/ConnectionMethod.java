package by.training.homework13;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class for connecting and working with https://jsonplaceholder.typicode.com
 * with usage of HttpURLConnection.
 */
public class ConnectionMethod implements HttpMethods {
    @Override
    public void post(int id) {
        Post post = new Post(id);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            URL url = new URL("https://jsonplaceholder.typicode.com/posts");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            byte[] json = objectMapper.writeValueAsBytes(post);
            OutputStream os = connection.getOutputStream();
            os.write(json);
            os.flush();
            os.close();
            post = objectMapper.readValue(connection.getInputStream(), Post.class);
            int responseCode = connection.getResponseCode();
            System.out.println("POST Response Code :  " + responseCode);
            System.out.println("POST Response Message : " + connection.getResponseMessage());
            System.out.println(post.toString());    
        } catch (IOException ex) {
            System.out.println("Can't connect to server.");
        }
    }
    
    @Override
    public void get(int id) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            URL url = new URL("https://jsonplaceholder.typicode.com/posts/" + id);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoInput(true);
            Post post = objectMapper.readValue(connection.getInputStream(), Post.class);
            System.out.println(post.toString());
        } catch (IOException ex) {
            System.out.println("Can't connect to server.");
        }
    }
}
