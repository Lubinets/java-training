package by.training.homework13;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * Class for connecting and working with https://jsonplaceholder.typicode.com
 * with usage of HttpClient.
 */
public class HttpClientMethod implements HttpMethods {
    @Override
    public void post(int id) {
        Post post = new Post(id);
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("https://jsonplaceholder.typicode.com/posts");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(post);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse response = client.execute(httpPost);
            StatusLine status = response.getStatusLine();
            post = objectMapper.readValue(response.getEntity().getContent(), Post.class);
            System.out.println("POST Response Code :  " + status.getStatusCode());
            System.out.println("POST Response Message : " + status.getReasonPhrase());
            System.out.println(post.toString());
        } catch (IOException ex) {
            System.out.println("Can't connect to server.");
        }
    }
    
    @Override
    public void get(int id) {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("https://jsonplaceholder.typicode.com/posts/" + id);
        try {
            HttpResponse response = client.execute(request);
            Post post = objectMapper.readValue(response.getEntity().getContent(), Post.class);
            System.out.println(post.toString());
            
        } catch (IOException ex) {
            System.out.println("Can't connect to server.");
        }
    }
}
