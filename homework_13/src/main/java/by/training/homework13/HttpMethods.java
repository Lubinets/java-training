package by.training.homework13;

/**
 * Interface for creating implementation of POST and GET methods to connect to
 * https://jsonplaceholder.typicode.com.
 */
public interface HttpMethods {
    /**
     * Implementation of method POST. Tries to create post with set id at
     * https://jsonplaceholder.typicode.com/posts
     * @param id id of the post to create
     */
    public void post(int id);
    
    /**
     * Implementation of method GET. Tries to get post with set id from
     * https://jsonplaceholder.typicode.com/posts
     * @param id id of the post to get
     */
    public void get(int id);
}
