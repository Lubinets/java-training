package by.training.homework13;

public class Main {
    /**
     * @param args the command line arguments. 
     * args[0] 1 - Connection, 2 - HttpClient. 
     * args[1] GET for GET method, POST for POST method.
     * args[2] ID of post at https://jsonplaceholder.typicode.com
     */
    public static void main(String[] args) {
        try {
            String connectionMethod = args[0];
            String httpMethod = args[1];
            int id = Integer.valueOf(args[2]);
            
            HttpMethods method = null;
            switch (connectionMethod) {
                case ("Connection"):
                    method = new ConnectionMethod();
                    break;
                case ("HttpClient"):
                    method = new HttpClientMethod();
                    break;
                default: {
                    System.out.println("Invalid connection method");
                    return;
                }
            }
            switch (httpMethod) {
                case ("GET"):
                    method.get(id);
                    break;
                case ("POST"):
                    method.post(id);
                    break;
                default:
                    System.out.println("Invalid HTTP method");
                    break;
            }        
        } catch (NumberFormatException ex) {
            System.out.println("Wrong input format");
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Wrong number of arguments");
        }      
    }
}
