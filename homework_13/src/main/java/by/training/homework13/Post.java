package by.training.homework13;

/**
 * Class representation of a forum post for json mapping.
 */
public class Post {
    private int userId;
    private int id;  
    private String title;
    private String body;
    
    public Post() {
        
    }
    
    public Post(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }
    
    public Post(int id) {
        this.id = id;
        userId = 5;
        title = "test";
        body = "test testing test testtest";
    }

    /**
     * Setter for parameter userId.
     * @param userId parameter to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Setter for parameter id.
     * @param id parameter to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Setter for parameter title.
     * @param title parameter to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Setter for parameter body.
     * @param body parameter to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * Getter for parameter userId.
     * @return returns parameter userId.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Getter for parameter id.
     * @return returns parameter id
     */
    public int getId() {
        return id;
    }

    /**
     * Getter for parameter title.
     * @return returns parameter title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter for parameter body.
     * @return returns parameter body
     */
    public String getBody() {
        return body;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Article [");
        builder.append(id);
        builder.append("] User [");
        builder.append(userId);
        builder.append("] Title [");
        builder.append(title);
        builder.append("] Message [");
        builder.append(body);
        builder.append("]");
        return builder.toString();
    }
}
