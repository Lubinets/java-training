package by.training.homework23.controllers;

import by.training.homework23.model.User;
import by.training.homework23.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    @Autowired
    LoginService service;
    
    @GetMapping(value = "/after-login")
    public String createSession(HttpServletRequest request, HttpSession session) {
        Optional<User> curentUser = service.getUserByName(request.getUserPrincipal().getName());
        curentUser.ifPresent((user) -> {
            int orderId = service.createOrder(user);
            session.setAttribute("terms", "true");
            session.setAttribute("name", user.getName());
            session.setAttribute("userId", user.getId());
            session.setAttribute("orderId", orderId);
        });
        return "redirect:/shop";
    }
    
    @GetMapping("/login")
    public Model getLoginPage(Model model) {
        return model;
    }
}
