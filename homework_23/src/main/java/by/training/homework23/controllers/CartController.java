package by.training.homework23.controllers;

import by.training.homework23.model.Product;
import by.training.homework23.service.OrderService;
import by.training.homework23.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import javax.servlet.http.HttpSession;

@Controller
public class CartController {
    @Autowired
    private OrderService orderService;
    
    @Autowired
    private ProductService productService;
    
    @GetMapping(value = "/cart")
    public Model getUserCart(Model model, HttpSession session) {
        int orderId = (Integer) session.getAttribute("orderId");
        List<Product> selectedProducts = productService.getSelectedProducts(orderId);
        if (!selectedProducts.isEmpty()) {
            model.addAttribute("cost", orderService.updateOrderPrice(orderId));
            model.addAttribute("emptyCart", "false");
        } else {
            model.addAttribute("emptyCart", "true");
        }
        
        model.addAttribute("selectedItems", selectedProducts);
        return model;
    }
}
