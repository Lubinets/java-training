package by.training.homework23.repository;

import by.training.homework23.model.Order;

import java.util.Optional;

/**
 * Dao to work with Orders table.
 */
public interface OrderDao {
    /**
     * Save order into database.
     * @param order order to save
     * @return returns id of the saved order
     */
    public int save(Order order);

    /**
     * Gets order by id.
     * @param id id of the order
     * @return optional of order
     */
    public Optional<Order> get(int id);
}
