package by.training.homework23.repository.impl;

import by.training.homework23.model.Product;
import by.training.homework23.repository.ProductDao;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Implementation of {@link ProductDao} for H2 database.
 */
@Repository
public class ProductDaoH2 implements ProductDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Optional get(int id) {
        return Optional.ofNullable(
                sessionFactory.getCurrentSession()
                .get(Product.class, id));
    }

    @Override
    public List<Product> getAll() {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Product", Product.class);
        return query.list();
    }
}
