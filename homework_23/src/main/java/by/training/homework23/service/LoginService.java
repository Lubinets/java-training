package by.training.homework23.service;

import by.training.homework23.model.Order;
import by.training.homework23.model.User;
import by.training.homework23.repository.OrderDao;
import by.training.homework23.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class LoginService {
    @Autowired
    UserDao userDao;
    
    @Autowired
    OrderDao orderDao;
    
    /**
     * Gets user from database by his login.
     * @param name login of the user
     * @return returns optional of user
     */
    public Optional<User> getUserByName(String name) {
        return userDao.getByName(name);
    }
    
    /**
     * Creates new order for user.
     * @param user user of thr order
     * @return returns id of the createad order
     */
    public int createOrder(User user) {
        Order order = new Order(0, 0);
        order.setUser(user);
        return orderDao.save(order);
    }
}
