package by.training.homework23.service;

import by.training.homework23.model.Order;
import by.training.homework23.model.Product;
import by.training.homework23.repository.OrderDao;
import by.training.homework23.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;
    
    @Autowired
    private ProductDao productDao;
    
    
    /**
     * Updates price of the order.
     * @param orderId id of the order
     * @return new price of the order
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public double updateOrderPrice(int orderId) {
        double totalPrice = 0;
        Optional<Order> orderOpt = orderDao.get(orderId);
        if (orderOpt.isPresent()) {
            Order order = orderOpt.get();
            for (Product product : order.getProducts()) {
                totalPrice += product.getProductPrice();
            }
            order.setTotalPrice(totalPrice);
        }      
        return totalPrice;
    }
    
    /**
     * Adds product to cart.
     * @param orderId id of the order
     * @param productId id of the product
     */
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void addToCart(int orderId, int productId) {
        Optional<Order> orderOpt = orderDao.get(orderId);
        Optional<Product> productOpt = productDao.get(productId);
        if (orderOpt.isPresent() && productOpt.isPresent()) {
            Order order = orderOpt.get();
            order.addProduct(productOpt.get());
        }
    }
}
