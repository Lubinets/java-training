CREATE TABLE User
(
    ID INTEGER not null,
    login VARCHAR(30) UNIQUE,
    password VARCHAR(15),
    PRIMARY KEY (ID)
);

CREATE TABLE Product
(
    ID INTEGER not null,
    title VARCHAR(30),
    price DOUBLE,
    PRIMARY KEY (ID)
);

CREATE TABLE Orders
(
    ID INTEGER AUTO_INCREMENT,
    user_id INTEGER,
    total_price DOUBLE,
    PRIMARY KEY (ID),
    FOREIGN KEY (user_id) REFERENCES User(ID),
);

CREATE TABLE Order_Good
(
    ID INTEGER AUTO_INCREMENT,
    order_id INTEGER,
    good_id INTEGER,
    PRIMARY KEY (ID),
    FOREIGN KEY (order_id) REFERENCES Orders(ID),
    FOREIGN KEY (good_id) REFERENCES Product(ID)
);

INSERT INTO User VALUES (1, 'User_1', '0');
INSERT INTO User VALUES (2, 'User_2', '0');

INSERT INTO Product VALUES (1, 'Book', 5.5);
INSERT INTO Product VALUES (2, 'Mobile phone', 10.0);
INSERT INTO Product VALUES (3, 'First test obj', 4.5);
INSERT INTO Product VALUES (4, 'Second test obj', 11.0);
INSERT INTO Product VALUES (5, 'Third test obj', 6.0);

