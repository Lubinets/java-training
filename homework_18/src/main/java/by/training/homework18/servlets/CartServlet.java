package by.training.homework18.servlets;

import by.training.homework18.OrderGood;
import by.training.homework18.Product;
import by.training.homework18.service.OrderDaoH2;
import by.training.homework18.service.OrderGoodDaoH2;
import by.training.homework18.service.ProductDaoH2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for creating web-page with users cart in the online shop.
 */
@WebServlet(name = "CartServlet", urlPatterns = {"/cart"})
public class CartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ProductDaoH2 productDao = new ProductDaoH2();
        OrderGoodDaoH2 orderGoodDao = new OrderGoodDaoH2();
        OrderDaoH2 orderDao = new OrderDaoH2();
        
        int orderId = (Integer) request.getSession().getAttribute("orderId");
        ArrayList<Product> selectedProduct = new ArrayList();
        List<OrderGood> orderGoods = orderGoodDao.getByOrderId(orderId);
        if (!orderGoods.isEmpty()) {
            double totalPrice = 0;
            for (OrderGood good : orderGoods) {
                Optional<Product> productOpt = productDao.get(good.getGoodId());
                if (productOpt.isPresent()) {
                    Product product = productOpt.get();
                    selectedProduct.add(product);
                    totalPrice += product.getProductPrice();
                }
            }
            orderDao.updateTotalPrice(orderId, totalPrice);
            request.setAttribute("cost", totalPrice);
            request.setAttribute("emptyCart", "false");
        } else {
            request.setAttribute("emptyCart", "true");
        }
        
        orderDao.close();
        orderGoodDao.close();
        productDao.close();
        
        request.setAttribute("selectedItem", selectedProduct);
        request.getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    } // </editor-fold>
}
