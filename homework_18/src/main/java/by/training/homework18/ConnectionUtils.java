package by.training.homework18;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectionUtils {
    private final String PROPERTIES_PATH = "src/main/resources/db/database.properties";
    private final String INIT_SCRIPT_PATH = "src/main/resources/db/init/init.sql";
            
    private static boolean isDbInitialized = false;
    
    /**
     * Connects to the h2 database.
     * @return return Connection to the database
     * @throws IOException if file with database properties not found 
     * or there is an error occurred while reading it
     * @throws SQLException if an error occured while trying to create connection
     * to database
     */
    public Connection getH2Connection() throws IOException, SQLException {
        FileInputStream input = new FileInputStream(PROPERTIES_PATH);
        Properties prop = new Properties();
        prop.load(input);
        String url = prop.getProperty("db.url");
        return DriverManager.getConnection(url);
    }
    
    /**
     * Runs script to initialize the database.
     */
    public void initDatabase() {
        try (Connection connection = getH2Connection()) {
            ScriptRunner runner = new ScriptRunner(connection);
            try (FileInputStream stream = new FileInputStream(INIT_SCRIPT_PATH)) {
                runner.runScript(new InputStreamReader(stream));
                runner.closeConnection();
                isDbInitialized = true;
            }
        } catch (IOException | SQLException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @return gets isIniatialized.
     */
    public boolean isDbInitialized() {
        return isDbInitialized;
    }
}
