package by.training.homework18.service;

import by.training.homework18.ConnectionUtils;
import by.training.homework18.Product;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of {@link ProductDao} for H2 database.
 */
public class ProductDaoH2 implements ProductDao {
    private final String GET_ALL = "SELECT * FROM Product";
    private final String GET = "SELECT * FROM PRODUCT WHERE ID = ?";

    private Connection connection;

    public ProductDaoH2() {
        try {
            ConnectionUtils utils = new ConnectionUtils();
            connection = utils.getH2Connection();
        } catch (IOException | SQLException ex) {
            Logger.getLogger(ProductDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Optional get(int id) {
        try (PreparedStatement st = connection.prepareStatement(GET)) {
            st.setInt(1, id);
            ResultSet result = st.executeQuery();
            if (result.next()) {
                return Optional.of(new Product(
                        result.getInt(1), result.getString(2), result.getDouble(3)));
            } else {
                return Optional.empty();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    @Override
    public List<Product> getAll() {
        try (Statement st = connection.createStatement()) {
            ResultSet result = st.executeQuery(GET_ALL);
            ArrayList<Product> resultList = new ArrayList();
            while (result.next()) {
                resultList.add(new Product(
                        result.getInt(1), result.getString(2), result.getDouble(3)));
            }
            return resultList;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
