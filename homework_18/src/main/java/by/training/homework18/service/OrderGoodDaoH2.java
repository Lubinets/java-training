package by.training.homework18.service;

import by.training.homework18.ConnectionUtils;
import by.training.homework18.OrderGood;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of {@link OrderGoodDao} for H2 database.
 * @author Admin
 */
public class OrderGoodDaoH2 implements OrderGoodDao {
    private final String SAVE = "INSERT INTO Order_Good (order_id, good_id) VALUES (? , ?)";
    private final String GET_BY_ORDER_ID = "SELECT * FROM Order_Good WHERE order_Id = ?";
    
    private Connection connection;
    
    public OrderGoodDaoH2() {
        ConnectionUtils utils = new ConnectionUtils();
        try {
            connection = utils.getH2Connection();
        } catch (IOException | SQLException ex) {
            Logger.getLogger(OrderGoodDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void save(OrderGood orderGood) {
        try (PreparedStatement st = connection.prepareStatement(SAVE)) {
            st.setInt(1, orderGood.getOrderId());
            st.setInt(2, orderGood.getGoodId());
            st.execute();
        } catch (SQLException ex) {
            Logger.getLogger(OrderGoodDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<OrderGood> getByOrderId(int orderId) {
        try (PreparedStatement st = connection.prepareStatement(GET_BY_ORDER_ID)) {
            st.setInt(1, orderId);
            ResultSet result = st.executeQuery();
            ArrayList<OrderGood> resultList = new ArrayList();
            while (result.next()) {
                resultList.add(new OrderGood(result.getInt(1), result.getInt(2), result.getInt(3)));
            }
            return resultList;
        } catch (SQLException ex) {
            Logger.getLogger(OrderGoodDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }
    
    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(OrderGoodDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
