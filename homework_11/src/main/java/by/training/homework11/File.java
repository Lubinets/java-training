package by.training.homework11;

import java.io.Serializable;
import java.util.Objects;

/**
 * An abstract representation of file.
 */
public class File implements Printable, Serializable {
    private String fileName;

    public File(final String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets file name with indent.
     * @param indent indent to add
     * @return returns file name with indent
     */
    public String print(final String indent) {
        return indent + fileName + "\n";
    }

    /**
     * Checks if input string is a file name.
     * @param input input string with name
     * @return returns true if file name, returns false otherwise
     */
    public static boolean isFileName(final String input) {
        return input.matches("(\\w+)(\\.{1})(\\w+)");
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof File)) {
            return false;
        }
        File file = (File) o;
        return this.fileName.equals(file.fileName);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.fileName);
        return hash;
    }
}
