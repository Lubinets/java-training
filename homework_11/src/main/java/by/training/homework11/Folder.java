package by.training.homework11;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * An abstract representation of directory.
 */
public class Folder implements Printable, Serializable {
    private String folderName;
    private List<Printable> folderElements;

    public Folder(final String folderName) {
        this.folderName = folderName;
        folderElements = new ArrayList<>();
    }

    /**
     * Adds a file or folder to the directory.
     * @param child child to add
     */
    public void addChild(final Printable child) {
        folderElements.add(child);
    }

    /**
     * Creates formatted string represention of sub catalogs and files in the
     * directory.
     * @param indent Indent used in formating. Pass empty string for root folder
     * @return return formatted string with folder name and names of its children
     */
    public String print(String indent) {
        StringBuilder builder = new StringBuilder();
        builder.append(indent);
        builder.append(folderName);
        builder.append("\n");
        indent += " ";
        for (Printable file : folderElements) {
            builder.append(file.print(indent));
        }
        return builder.toString();
    }

    /**
     * Gets index of a sub element with set name.
     * If element not found returns -1.
     * @param name name of folder to search
     * @return returns index of a folder or -1 if not found
     */
    public int getChildIndex(final String name) {
        if (File.isFileName(name)) {
            return folderElements.indexOf(new File(name));
        } else {
            return folderElements.indexOf(new Folder(name));
        }
    }

    /**
     * Gets instance of folders element by its index.
     * @param index index of element
     * @return returns instance of sub folder or file
     */
    public Printable getChild(final int index) {
        return folderElements.get(index);
    }
    
    /**
     * Saves file structure to file on a hard drive.
     * @param fileName name of the file
     */
    public void toFile(String fileName) {
        if (fileName == null) {
            throw new IllegalArgumentException();
        }
        try (ObjectOutputStream objOut = new ObjectOutputStream(
                new FileOutputStream(fileName))) {
            objOut.writeObject(this);
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Folder)) {
            return false;
        }
        Folder folder = (Folder) o;
        return this.folderName.equals(folder.folderName);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.folderName);
        return hash;
    }
}
