<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to online shop</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="block-middle">
            <h1>Hello ${name}!</h1>
            <div> You have already chosen:</div>
            <ol>
                <c:forEach items="${selectedItems}" var="selected">
                    <li><c:out value="${selected.productName} ${selected.productPrice}"/>$
                </c:forEach>
            </ol>
            <form method="GET" action="add-to-cart">
                <select required name="selectedItem">
                    <c:forEach items="${goodsList}" var="product">
                        <option value="${product.id}"><c:out value="${product.productName} ${product.productPrice}"/>$</option>
                    </c:forEach>
                </select> </br>
                <input type=submit value='Add item'> </br>
            </form>
            <input onclick=window.location.href="cart" type='button' value='Submit'>
        </div>
    </body>
</html>
