<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to online shop</title>
    </head>
    <body>
        <h1>OOPS!</h1>
        <div>
            You shouldn't be here. </br>
            Please, agree with the terms of service first. </br>
            <a href='../homework_19'>Start page</a>
        </div>
    </body>
</html>
