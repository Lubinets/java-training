package by.training.homework19.service;

import by.training.homework19.OrderGood;
import by.training.homework19.Product;
import by.training.homework19.repository.OrderDao;
import by.training.homework19.repository.OrderGoodDao;
import by.training.homework19.repository.ProductDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class OrderService {
    private OrderDao orderDao;
    private OrderGoodDao orderGoodDao;
    private ProductDao productDao;
    
    private double totalPrice = 0;
    
    /**
     * Returns list of products from the selected order.
     * @param orderId id of the order
     * @return list of products
     */
    public List<Product> getAllOrderedProductsById(int orderId) {
        ArrayList<Product> selectedProducts = new ArrayList();
        List<OrderGood> orderGoods = orderGoodDao.getByOrderId(orderId);
        if (!orderGoods.isEmpty()) {
            for (OrderGood good : orderGoods) {
                Optional<Product> productOpt = productDao.get(good.getGoodId());
                if (productOpt.isPresent()) {
                    Product product = productOpt.get();
                    selectedProducts.add(product);
                    totalPrice += product.getProductPrice();
                }
            }
            orderDao.updateTotalPrice(orderId, totalPrice);
        }
        orderDao.close();
        orderGoodDao.close();
        productDao.close();
        
        return selectedProducts;
    }
    
    /**
     * @param orderDao the orderDao to set
     */
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * @param orderGoodDao the orderGoodDao to set
     */
    public void setOrderGoodDao(OrderGoodDao orderGoodDao) {
        this.orderGoodDao = orderGoodDao;
    }

    /**
     * @param productDao the productDao to set
     */
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    /**
     * Returns total price of the product selected by {@link getAllOrderedProductsById()}.
     * Returns 0 if {@link getAllOrderedProductsById()} has not been use yet.
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }
}
