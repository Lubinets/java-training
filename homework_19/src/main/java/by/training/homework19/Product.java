package by.training.homework19;


public class Product {
    private int id;
    private String productName; 
    private double productPrice;
    
    public Product(int id, String productName, double productPrice) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
    }
    
    /**
     * Getter for productName.
     * @return returns product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Getter for productPrice.
     * @return returns product price
     */
    public double getProductPrice() {
        return productPrice;
    }
    
    /**
     * Setter for productName.
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Setter for productPrice.
     * @param productPrice the productPrice to set
     */
    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
