package by.training.homework19.servlets;

import by.training.homework19.OrderGood;
import by.training.homework19.Product;
import by.training.homework19.repository.OrderGoodDao;
import by.training.homework19.repository.ProductDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for choosing items to buy from the online shop.
 */
@WebServlet(name = "ShopServlet", urlPatterns = {"/shop"})
public class ShopServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ApplicationContext context  = new ClassPathXmlApplicationContext(
                "/spring/SpringXMLConfig.xml");
        ProductDao productDao = (ProductDao) context.getBean("ProductDao");
        OrderGoodDao orderDao = (OrderGoodDao) context.getBean("OrderGoodDao");
        
        int orderId = (Integer) request.getSession().getAttribute("orderId");
        ArrayList<Product> selectedProducts = new ArrayList();
        List<OrderGood> orderGoods = orderDao.getByOrderId(orderId);
        for (OrderGood good : orderGoods) {
            Optional<Product> productOpt = productDao.get(good.getGoodId());
            productOpt.ifPresent((product) -> {
                selectedProducts.add(product);
            });
        }
        request.setAttribute("goodsList", productDao.getAll());
        request.setAttribute("selectedItems", selectedProducts);
        productDao.close();
        orderDao.close();
        request.getRequestDispatcher("/WEB-INF/views/shop.jsp").forward(request, response);    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    } // </editor-fold>
}
