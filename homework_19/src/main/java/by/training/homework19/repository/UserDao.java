package by.training.homework19.repository;

import by.training.homework19.User;

import java.util.Optional;

/**
 * Dao for User table.
 */
public interface UserDao {
    /**
     * Gets user by id from the database.
     * @param id id of the user
     * @return return optional of user
     */
    public Optional<User> get(int id);
    
    /**
     * Gets user by login from the database.
     * @param login
     * @return return optional of user
     */
    public Optional<User> getByName(String login);
    
    /**
     * Closes the connection to database.
     */
    public void close();
}
