package by.training.homework19.repository;

import by.training.homework19.ConnectionUtils;
import by.training.homework19.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;


/**
 * Implementation of {@link UserDao} for H2 database.
 */
public class UserDaoH2 implements UserDao {
    private final String GET_BY_NAME = "SELECT ID, login FROM User WHERE login = ?";
    private final String GET = "SELECT ID, login FROM User WHERE ID = ?";
    
    private Connection connection;
    
    @Override
    public Optional get(int id) {
        try (PreparedStatement st = connection.prepareStatement(GET)) {
            st.setInt(1, id);
            ResultSet result = st.executeQuery();
            if (result.next()) {
                return Optional.of(new User(result.getInt(1), result.getString(2), ""));
            } else {
                return Optional.empty();
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }
    
    @Override
    public Optional getByName(String login) {
        try (PreparedStatement st = connection.prepareStatement(GET_BY_NAME)) {
            st.setString(1, login);
            ResultSet result = st.executeQuery();
            if (result.next()) {
                return Optional.of(new User(result.getInt(1), result.getString(2), ""));
            } else {
                return Optional.empty();
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    /**
     * @param source factory for the connection.
     */
    public void setConnection(DataSource source) {
        try {
            connection = source.getConnection();
            if (!ConnectionUtils.isDbInitialized()) {
                ConnectionUtils.initDatabase(connection);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
