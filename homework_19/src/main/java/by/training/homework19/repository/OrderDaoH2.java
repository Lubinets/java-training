package by.training.homework19.repository;

import by.training.homework19.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;


/**
 * Implementation of {@link OrderDao} for H2 database. 
 */
public class OrderDaoH2 implements OrderDao {
    private final String UPDATE_PRICE = "UPDATE Orders SET total_price = ? WHERE ID = ?";
    private final String SAVE = "INSERT INTO Orders (user_id, total_price) VALUES(? , ?)";
    
    private Connection connection;

    @Override
    public int save(Order order) {
        try (PreparedStatement st = connection.prepareStatement(
                SAVE, Statement.RETURN_GENERATED_KEYS)) {
            st.setInt(1, order.getUserId());
            st.setInt(2, 0);
            st.executeUpdate();
            ResultSet result = st.getGeneratedKeys();
            result.next();
            int orderId = result.getInt(1);
            return orderId;
        } catch (SQLException ex) {
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    
    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void updateTotalPrice(int id, double totalPrice) {
        try (PreparedStatement st = connection.prepareStatement(UPDATE_PRICE)) {
            st.setDouble(1, totalPrice);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param source factory for the connection.
     */
    public void setConnection(DataSource source) {
        try {
            connection = source.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
