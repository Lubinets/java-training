package by.training.homework19;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectionUtils {
    private static final String INIT_SCRIPT_PATH = "src/main/resources/db/init/init.sql";           
    private static boolean isDbInitialized = false;
    
    /**
     * Runs script to initialize the database.
     * @param connection connection to the database
     */
    public static void initDatabase(Connection connection) {
        ScriptRunner runner = new ScriptRunner(connection);
        try (FileInputStream stream = new FileInputStream(INIT_SCRIPT_PATH)) {
            runner.runScript(new InputStreamReader(stream));
            isDbInitialized = true;
        } catch (IOException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @return gets isIniatialized.
     */
    public static boolean isDbInitialized() {
        return isDbInitialized;
    }
}
