/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Admin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number;
        int algorithmId;
        int loopId;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter algorithmID: ");
        algorithmId = Integer.parseInt(in.nextLine());
        System.out.print("Enter loopID: ");
        loopId = Integer.parseInt(in.nextLine());
        System.out.print("Enter number: ");
        number = Integer.parseInt(in.nextLine());
        if (algorithmId == 1) {
            System.out.print(Arrays.toString(
                    Algorithms.fibonacci(number, loopId)));
        } else if (algorithmId == 2) {
            System.out.print(Algorithms.factorial(number, loopId));
        } else {
            System.out.print("AlgorithmID not found");
        }
    }
}
