/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import java.util.Objects;

/**
 *
 * @author Admin
 */
public final class Algorithms {
    /**
     * Non-used constructor for utility class.
     */
    private Algorithms() { }

    /**
     * Gets first n elements from fibonacci sequence.
     * @param number selects number of elements to get
     * @param loopType selects loop type for algorithm
     * where 1 = while, 2 - do/while, 3 - for
     * @return returns array of first n elements from fibonacci sequence
     */
    public static int[] fibonacci(final Integer number, final Integer loopType) {
        if (Objects.isNull(number) || number < 1) {
            throw new IllegalArgumentException();
        }
        int[] result = new int[number];
        int i = 2;
        result[0] = 0;
        if (number == 1) {
            return result;
        }
        result[1] = 1;
        if (number == 2) {
            return result;
        }
        switch (loopType) {
            case (1):
                while (i < number) {
                    result[i] = result[i - 1] + result[i - 2];
                    i++;
                }
                break;
            case (2):
                do {
                    result[i] = result[i - 1] + result[i - 2];
                    i++;
                }
                while (i < number);
                break;
            case (3):
                for (; i < number; i++) {
                    result[i] = result[i - 1] + result[i - 2];
                }
                break;
            default: 
                throw new IllegalArgumentException("Invalid LoopID");
        }
        return result;
    }

    /**
     * Computes factorial of a number.
     * @param number sets a number to get its factorial
     * @param loopType selects loop type for algorithm
     * where 1 = while, 2 - do/while, 3 - for
     * @return returns factorial of the set number
     */
    public static int factorial(final Integer number, final Integer loopType) {
        if (Objects.isNull(number)) {
            throw new IllegalArgumentException();
        }
        int result = 1;
        int i = 1;
        switch (loopType) {
            case (1):
                while (i <= number) {
                    result *= i;
                    i++;
                }
                break;
            case (2):
                do {
                    result *= i;
                    i++;
                }
                while (i <= number);
                break;
            case (3):
                for (; i <= number; i++) {
                    result *= i;
                }
                break;
            default:
                throw new IllegalArgumentException("Invalid LoopID");
        }
        return result;
    }
}
