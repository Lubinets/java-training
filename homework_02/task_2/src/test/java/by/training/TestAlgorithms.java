package by.training;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import by.training.Algorithms;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class TestAlgorithms {
    @Test
    public void testFibonacci() {
        int[] expected = {0, 1, 1, 2, 3, 5, 8, 13};
        int[] actual = Algorithms.fibonacci(8, 1);
        Assert.assertArrayEquals(expected, actual);
        actual = Algorithms.fibonacci(8, 2);
        Assert.assertArrayEquals(expected, actual);
        actual = Algorithms.fibonacci(8, 3);
        Assert.assertArrayEquals(expected, actual);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFibonacciWithNull() {
        int[] expected = {0, 1, 1, 2, 3, 5, 8, 13};
        int[] actual = Algorithms.fibonacci(null, 1);
        Assert.assertArrayEquals(expected, actual);
        actual = Algorithms.fibonacci(8, null);
        Assert.assertArrayEquals(expected, actual);
    }
    
    @Test
    public void testFactorial() {
        int expected = 120;
        int actual = Algorithms.factorial(5, 1);
        Assert.assertEquals(expected, actual);
        actual = Algorithms.factorial(5, 2);
        Assert.assertEquals(expected, actual);
        actual = Algorithms.factorial(5, 3);
        Assert.assertEquals(expected, actual);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFactorialWithNull() {
        int expected = 13;
        int actual = Algorithms.factorial(null, 1);
        Assert.assertEquals(expected, actual);
        actual = Algorithms.factorial(5, null);
        Assert.assertEquals(expected, actual);
    }
}
