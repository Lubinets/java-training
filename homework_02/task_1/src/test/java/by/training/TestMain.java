package by.training;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import by.training.Main;
import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author Admin
 */
public class TestMain {
    @Test
    public void testGetG() {
        double expected = 29.283;
        double actual = Main.getG(3, 2, 3.5, 5.6);
        Assert.assertEquals(expected, actual, 0.001);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testGetGWithNull() {
        double expected = 29.283;
        double actual = Main.getG(3, 2, null, 5.6);
        Assert.assertEquals(expected, actual, 0.001);
    }
}
