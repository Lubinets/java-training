/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import static java.lang.Math.PI;
import java.util.Objects;

/**
 *
 * @author Admin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double result = getG(
                Integer.parseInt(args[0]), Integer.parseInt(args[1]),
                Double.parseDouble(args[2]), Double.parseDouble(args[3]));
        System.out.print(result);
    }

    /**
     * Computes expression G=4 * PI^2 * a^3 /
     * (p^2 * (m1 + m2)).
     * @param a first element of expression
     * @param p second element of expression
     * @param m1 third element of expression
     * @param m2 fourth element of expression
     * @return returns G - result of an expression
     */
    public static double getG(Integer a, Integer p, Double m1, Double m2) {
        if (Objects.isNull(a) || Objects.isNull(p)
                || Objects.isNull(m1) || Objects.isNull(m2)) {
            throw new IllegalArgumentException();
        }
        return 4 * Math.pow(PI, 2) * (Math.pow(a, 3))
                / (Math.pow(p, 2) * (m1 + m2));
    }
}
