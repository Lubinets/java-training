package by.training.homework24.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Product")
public class Product implements Serializable {
    @Id
    @Column(name = "ID")
    @GeneratedValue  
    private int id;
    
    @Column(name = "title")
    private String productName;
    
    @Column(name = "price")
    private double productPrice;
    
    public Product(int id, String productName, double productPrice) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
    }

    public Product() {

    }
    
    /**
     * Getter for productName.
     * @return returns product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Getter for productPrice.
     * @return returns product price
     */
    public double getProductPrice() {
        return productPrice;
    }
    
    /**
     * Setter for productName.
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Setter for productPrice.
     * @param productPrice the productPrice to set
     */
    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
