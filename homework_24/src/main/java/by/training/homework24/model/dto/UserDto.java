package by.training.homework24.model.dto;



public class UserDto {
    private String name;
    
    
    public UserDto() {
        
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
