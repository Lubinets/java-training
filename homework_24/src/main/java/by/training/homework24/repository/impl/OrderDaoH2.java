package by.training.homework24.repository.impl;

import by.training.homework24.model.Order;
import by.training.homework24.repository.OrderDao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Implementation of {@link OrderDao} for H2 database. 
 */
@Repository
public class OrderDaoH2 implements OrderDao {   
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int save(Order order) {
       return (Integer) sessionFactory.getCurrentSession()
                .save(order);
    }
    
    @Override
    public Optional<Order> get(int id) {
        return Optional.ofNullable(
                sessionFactory.getCurrentSession()
                .get(Order.class, id));
    }
}
