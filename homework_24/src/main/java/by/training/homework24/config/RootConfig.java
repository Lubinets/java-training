package by.training.homework24.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "by.training.homework24")
public class RootConfig {   
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }   
}
