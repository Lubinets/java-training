package by.training.homework24.config.security;

import by.training.homework24.model.Order;
import by.training.homework24.service.LoginService;
import by.training.homework24.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderSecurity {
    @Autowired
    private LoginService service;
    
    @Autowired
    private OrderService orderService;
    
    public boolean hasAccess(Authentication authentication, int orderId) {
        try {
            User user = (User) authentication.getPrincipal();
            Optional<Order> orderOpt = orderService.findOrderById(orderId);
            if (orderOpt.isPresent()) {
                if (user.getUsername().equals(orderOpt.get().getUser().getName())) {
                    return true;
                } else {
                    return false;
                }
            }  else {
                    return false;
            }
        } catch(Exception ex) {
            return false;
        }
    }
}
