package by.training.homework24.controllers;



import by.training.homework24.model.Order;
import by.training.homework24.model.Product;
import by.training.homework24.model.dto.OrderDto;
import by.training.homework24.service.OrderService;
import by.training.homework24.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(path = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {
    private OrderService orderService;
    
    private ProductService productService;
    
    private ModelMapper modelMapper;
    
    @Autowired
    public OrderController(OrderService orderService, 
            ProductService productService, ModelMapper modelMapper) {
        this.orderService = orderService;
        this.productService = productService;
        this.modelMapper = modelMapper;
    }
        
    @GetMapping(path = "/{orderId}")
    public ResponseEntity<OrderDto> getOrder (@PathVariable(value = "orderId") int id) {
        Optional<Order> orderOpt = orderService.findOrderById(id);
        if (orderOpt.isPresent()) {
            return ResponseEntity.ok(
            modelMapper.map(orderOpt.get(), OrderDto.class));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @PutMapping("/{orderId}")
    public @ResponseBody ResponseEntity<Void> addToCart(@PathVariable("orderId") int id, 
            @RequestParam("productId") int productId) {
        Optional<Product> productOpt = productService.getById(productId);
        if(productOpt.isPresent()) {
            orderService.addToCart(id, productOpt.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    
    @PutMapping(path = "/{orderId}/checkout")
    public ResponseEntity<OrderDto> checkout(@PathVariable(value = "orderId") int id) {
        orderService.updateOrderPrice(id);
        Optional<Order> orderOpt = orderService.findOrderById(id);
        if (orderOpt.isPresent()) {
            return ResponseEntity.ok(
            modelMapper.map(orderOpt.get(), OrderDto.class));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
