package by.training.homework24.controllers;



import by.training.homework24.model.User;
import by.training.homework24.model.dto.UserDto;
import by.training.homework24.service.LoginService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    private LoginService loginService;

    private ModelMapper modelMapper;
    
    @Autowired
    public UserController(LoginService loginService, ModelMapper modelMapper) {
        this.loginService = loginService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity<UserDto> getUser(@PathVariable(name = "userId") int id) {
        Optional<User> userOpt = loginService.getUser(id);
        if (userOpt.isPresent()) {
            return ResponseEntity.ok(
                    modelMapper.map(userOpt.get(), UserDto.class));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/{userId}/orders")
    public ResponseEntity<Void> createOrder(@PathVariable(name = "userId") int id,
            UriComponentsBuilder builder) {
        Optional<User> userOpt = loginService.getUser(id);
        if (userOpt.isPresent()) {
            int orderId = loginService.createOrder(userOpt.get());
            UriComponents uriComponents = 
                    builder.path("/orders/{orderId}").buildAndExpand(orderId);
            return ResponseEntity.created(uriComponents.toUri()).build();
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }   
}
