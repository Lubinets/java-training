CREATE TABLE IF NOT EXISTS User
(
    ID INTEGER not null,
    login VARCHAR(30) UNIQUE,
    password VARCHAR(100),
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS Product
(
    ID INTEGER not null,
    title VARCHAR(30),
    price DOUBLE,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS Orders
(
    ID INTEGER AUTO_INCREMENT,
    user_id INTEGER,
    total_price DOUBLE,
    PRIMARY KEY (ID),
    FOREIGN KEY (user_id) REFERENCES User(ID)
);

CREATE TABLE IF NOT EXISTS Order_Good
(
    ID INTEGER AUTO_INCREMENT,
    order_id INTEGER,
    good_id INTEGER,
    PRIMARY KEY (ID),
    FOREIGN KEY (good_id) REFERENCES Product(ID),
    FOREIGN KEY (order_id) REFERENCES Orders(ID)
);

MERGE INTO User (ID, login, password) VALUES (1, 'User_1', '$2a$10$/OaK4hJffgA3.KgvTphxKObuXDk2e8WJS2vE0HgPxp54bHBenJUs6');
MERGE INTO User (ID, login, password) VALUES (2, 'User_2', '$2a$10$cFo.cz.GOpVLgm87N8t0W.XhZl2kWrjRXPAYv3C/UPAQUp8l5lM1y');

MERGE INTO Product (ID, title, price) VALUES (1, 'Book', 5.5);
MERGE INTO Product (ID, title, price) VALUES (2, 'Mobile phone', 10.0);
MERGE INTO Product (ID, title, price) VALUES (3, 'First test obj', 4.5);
MERGE INTO Product (ID, title, price) VALUES (4, 'Second test obj', 11.0);
MERGE INTO Product (ID, title, price) VALUES (5, 'Third test obj', 6.0);


