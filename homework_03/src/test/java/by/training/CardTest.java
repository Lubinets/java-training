/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class CardTest {
    
    @Test
    public void testAdding() {
        double expected = 15.32;
        Card card = new Card("test");
        card.add(15.32);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test
    public void testWithdraw() {
        double expected = 10.245;
        Card card = new Card("test", 20.245);
        card.withdraw(10.0);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
        expected = 0;
        card.withdrawAll();
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test
    public void testDisplayInOtherCurrency() {
        double expected = 4.7;
        Card card = new Card("test", 10.0);
        double actual = card.displayInOtherCurrency(0.47);
        Assert.assertEquals(expected, actual, 0.00001);
    }
    
    @Test
    public void testGetOwner() {
        String expected = "owner";
        Card card = new Card("owner");
        Assert.assertEquals(expected, card.getOwner());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddWithNegativeCurrency() {
        double expected = -10;
        Card card = new Card("test", 10.542);
        card.add(-20.542);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawWithNegativeCurrency() {
        double expected = 30.542;
        Card card = new Card("test", 10.542);
        card.withdraw(-20.0);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFirstConstructorWithNull() {
        String expectedName = "owner";
        Card card = new Card(null);
        Assert.assertEquals(expectedName, card.getOwner());   
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSecondConstructorWithNull() {
        double expectedBalance = 10.2;
        Card card = new Card(null, null);
        Assert.assertEquals(expectedBalance, card.showBalance(), 0.00001);
    }
       
    @Test(expected = IllegalArgumentException.class)
    public void testOtherCurrencyWithNull() {
        double expected = 4.7;
        Card card = new Card("test", 10.0);
        double actual = card.displayInOtherCurrency(null);
        Assert.assertEquals(expected, actual, 0.00001);
    }
}
