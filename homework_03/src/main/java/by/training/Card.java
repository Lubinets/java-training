/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import java.util.Objects;

/**
 *
 * @author Admin
 */
public class Card {
    /**
     * Money balance on card.
     */
    private double balance;

    /**
     * Name of the card owner.
     */
    private String owner;

    /**
     * Constructor for the class Card.
     * @param owner {@link Card#owner}
     */
    public Card(final String owner) {
        if (owner == null) {
            throw new IllegalArgumentException();
        }
        this.owner = owner;
        this.balance = 0;
    }

    /**
     * Constructor for the class Card.
     * @param owner {@link Card#owner}
     * @param balance {@link Card#balance}
     */
    public Card(final String owner, final Double balance) {
        if (owner == null || balance < 0 || Objects.isNull(balance)) {
            throw new IllegalArgumentException();
        }
        this.owner = owner;
        this.balance = balance;
    }

    /**
     * Adds money to the balance.
     * @param currency Amount of money to add
     */
    public void add(final Double currency) {
        if (currency < 0 || Objects.isNull(currency)) {
            throw new IllegalArgumentException();
        }
        balance += currency;
    }

    /**
     * Withdraw money from balance.
     * @param currency Amount of money to withdraw
     */
    public void withdraw(final Double currency) {
        if (currency < 0 || Objects.isNull(currency)) {
            throw new IllegalArgumentException();
        }
        balance -= currency;
    }

    /**
     * Withdraws all money from balance.
     */
    public void withdrawAll() {
        balance = 0;
    }

    /**
     * {@link Card#balance}.
     * @return returns card balance
     */
    public double showBalance() {
        return balance;
    }

    /**
     * {@link Card#owner}.
     * @return returns card owner name
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Shows balance of card in other currency.
     * @param exchangeRate exchange rate of currency
     * @return returns balance in other currency.
     */
    public double displayInOtherCurrency(final Double exchangeRate) {
        if (Objects.isNull(exchangeRate) || exchangeRate < 0) {
            throw new IllegalArgumentException();
        }
        return balance * exchangeRate;
    }
}
