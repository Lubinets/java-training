/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import java.util.Arrays;

/**
 *
 * @author Admin
 */
public final class Median {
    /**
     * Non-used constructor for utility class.
     */
    private Median() { };

    /**
     * Finds median of an array with integer elements.
     * @param array array to find median
     * @return returns median of an array
     */
    public static float median(final int[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        Arrays.sort(array);
        if (array.length % 2 == 1) {
            return array[array.length / 2];
        } else {
            return (float) 0.5 * (array[array.length / 2 - 1]
                    + array[array.length / 2]);
        }
    }

    /**
     * Finds median of an array with double elements.
     * @param array array to find median
     * @return returns median of an array
     */
    public static double median(final double[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        Arrays.sort(array);
        if (array.length % 2 == 1) {
            return array[array.length / 2];
        } else {
            return 0.5 * (array[array.length / 2 - 1]
                    + array[array.length / 2]);
        }
    }
}
