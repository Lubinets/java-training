/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training.homework_05;

import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class FileStructureBuilderTest {
    FileStructureBuilder builder;
    Folder expectedRoot;
    
    public FileStructureBuilderTest() {
        expectedRoot = new Folder("root");
        Folder test = new Folder("test");
        expectedRoot.addChild(test);
        test.addChild(new File("test.exe"));
        test.addChild(new Folder("testing"));
    }

    @Test
    public void testBuild() {
        String path = "test\\test.exe";
        builder = new FileStructureBuilder(path);
        Folder actualRoot = new Folder("root");
        builder.build(actualRoot);
        path = "test\\testing";
        builder = new FileStructureBuilder(path);
        builder.build(actualRoot);
        Assert.assertEquals(expectedRoot.print(""), actualRoot.print(""));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testBuildWithWrongInput() {
        String input = "files\\+-test file  ?folder";
        builder = new FileStructureBuilder(input);
        builder.build(expectedRoot);
    }
}
