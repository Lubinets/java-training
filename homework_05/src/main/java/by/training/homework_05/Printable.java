package by.training.homework_05;

/**
 * Interface to create formatted string representation of file system.
 */
public interface Printable {

    /**
     * Creates formatted string represention of element.
     * @param indent indent for element
     * @return returns formatted string represention of element
     */
    String print(String indent);
}
