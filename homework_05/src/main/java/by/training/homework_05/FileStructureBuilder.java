package by.training.homework_05;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class responsible for building file structure from string path.
 */
public class FileStructureBuilder {
    private String path;

    public FileStructureBuilder(final String input) {
        path = input;
    }

    /**
     * Checks path for validity.
     * @param input path to check
     * @return returns the result of verification
     */
    private boolean verifyInput(final String input) {
        Pattern p = Pattern.compile("[^/:*?<>|+]+");
        Matcher m = p.matcher(input);
        return m.matches();
    }

    /**
     * Builds file structure from path.
     * @param root root folder to create structure
     */
    public void build(final Folder root) {
        if (!verifyInput(path)) {
            throw new IllegalArgumentException();
        }
        String[] pathTokens = path.split("\\\\");
        Folder tmp = root;
        Printable element;
        for (String s : pathTokens) {
            int i = tmp.getChildIndex(s);
            if (i == -1) {
                if (File.isFileName(s)) {
                    element = new File(s);
                    tmp.addChild(element);
                    break;
                } else {
                    element = new Folder(s);
                    tmp.addChild(element);
                    tmp = (Folder) element;
                }
            } else {
                if (!File.isFileName(s)) {
                    tmp = (Folder) tmp.getChild(i);
                }
            }
        }
    }
}
