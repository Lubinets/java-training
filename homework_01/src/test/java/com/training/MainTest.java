/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training;


import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author Admin
 */
public class MainTest {
    
    @Test
    public void testSum(){
        int expected = 11;
        int actual = Main.getSum(5, 6);
        Assert.assertEquals(expected, actual);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSumWithNull(){
        int expected = 11;
        int actual = Main.getSum(null, 6);
        Assert.assertEquals(expected, actual);
    }
}
