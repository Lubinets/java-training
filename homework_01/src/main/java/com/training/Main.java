/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training;

import java.util.Objects;


public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello world");
    }
    
    public static int getSum (Integer a, Integer b){
        if (Objects.isNull(a) || Objects.isNull(b))
            throw new IllegalArgumentException();
        return a+b;
    }
    
}
