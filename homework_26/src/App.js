import React, { Component } from 'react';
import ListElement from './ListElement';
import Inputs from './Inputs';

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos: [],
    };

    this.deleteTodo = this.deleteTodo.bind(this);
    this.addTodo = this.addTodo.bind(this);
  }

  deleteTodo(id) {
    this.setState((prevState) => ({
      todos: prevState.todos.filter(todo => todo.id !== id),
    }))
  }

  addTodo(todo, description) {
    this.setState((prevState) => ({
      todos: [...prevState.todos, 
        {
          id: prevState.todos.length + 1,
          todo: todo, 
          description: description
        }
      ]
    }))
  }
   
  componentDidMount() {
    fetch("https://5cebe33277d47900143b8fa8.mockapi.io/todos")
    .then(results => {
      return results.json();
    }).then(data => {
      this.setState({todos: data});
      })
  }

  render() {
    const {todos} = this.state;
    return(
      <>
        <p>Add TODO</p>
        <Inputs onSubmit={this.addTodo}/>
        <ol>
          {todos.map(todo =>
            <ListElement key={todo.id} 
              id ={todo.id} 
              todo={todo.todo} 
              description={todo.description} 
              onDelete={ this.deleteTodo }
            />
          )}
        </ol>
      </>
    )
  }
}

export default App;
