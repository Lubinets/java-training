import React, { Component } from 'react';

class Inputs extends Component {
    constructor(props) {
        super(props);
        this.state = {
          todo: '',
          description: ''
        };

        this.onTodoChange = this.onTodoChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onTodoChange(event) {
        this.setState({todo: event.target.value});
    }

    onDescChange(event) {
        this.setState({description: event.target.value});
    }

    onSubmit(event) {
        event.preventDefault();
        this.props.onSubmit(this.state.todo, this.state.description)
    }

    render() {
        return(
            <form onSubmit={this.onSubmit}>
                <input onChange={this.onTodoChange} name='todo' type='text' placeholder='TODO'/>
                <input onChange={this.onDescChange} name='description' type='text' placeholder='Description'/>
                <input type='submit' value='Submit'/>
            </form>
        )
    }
}

export default Inputs;