import React, { Component } from 'react';
import Inputs from './Inputs';

class ListElement extends Component {
    constructor(props) {
        super(props);
        this.state = {
          showContent: false,
          showEdit: false,
          todo: this.props.todo,
          description: this.props.description
        };
        
        this.showContent = this.showContent.bind(this);
        this.showEdit = this.showEdit.bind(this);
        this.edit = this.edit.bind(this);
    }

    showContent() {
        this.setState({
            showContent: !this.state.showContent
        });
    }
    
    showEdit() {
        this.setState({
            showEdit: !this.state.showEdit
        });
    }

    edit(todo, description) {
        this.setState((prevState) => ({todo: todo, description: description}));
    }

    render() {
        return (
            <li>
                <div>{this.state.todo}</div>
                {this.state.showContent ? <div>{this.state.description}</div> : ''}
                <button onClick={this.showContent}>Show</button>
                <button onClick={() => this.props.onDelete(this.props.id)}>Delete</button>
                <button onClick={this.showEdit}>Edit</button>
                {this.state.showEdit ? <Inputs onSubmit={this.edit}/> : ''}
            </li>
        )
    }
}

export default ListElement;