$(document).ready(function() {
    var orderId = $('#orderId').val();

    loadOrder();
    
    $.ajax({
        url: 'http://localhost:8080/homework_28/products',
        dataType: 'json',
        type: 'GET'
    }).done(function(data) {
        $.each(data, function(key, value) {
                $("#selectedItem").append("<option value="+ value.id + ">" + value.productName + " " + value.productPrice + "$</option>");
        });
    });
    
    $('#addItem').click(function(){
       var value = $("#selectedItem").val();
       $.ajax({
          url: "http://localhost:8080/homework_28/orders/" + orderId + "?productId=" + value,
          type: "PUT"
       }).done(function() {
           loadOrder();
       });
    });
    
    function loadOrder() {
        $.ajax({
            url: 'http://localhost:8080/homework_28/orders/' + orderId,
            dataType: 'json',
            type: "GET"
        }).done(function(data) {
            $("#order").empty();
            $.each(data.products, function (key, value) {
                $("#order").append("<li>" + value.productName + " " + value.productPrice + "$</li>");
            });
        });
    };
});


