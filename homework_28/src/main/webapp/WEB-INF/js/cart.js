$(document).ready(function() {
    var orderId = $('#orderId').val();
    
    $.ajax({
        url: 'http://localhost:8080/homework_28/orders/'+ orderId + '/checkout',
        type: "PUT"
    }).done(function(data) {
        if(!$.isEmptyObject(data.products)) {
            $.each(data.products, function (key, value) {
                $('#order').append("<li>" + value.productName + " " + value.productPrice + "$</li>");
            });
            $('#totalPrice').append("Total price: " + data.totalPrice + "$");
        } else {
            $('#info').append("<p>You cart is empty</p>");
        }
    });
});
