package by.training.homework28.config.security;

import by.training.homework28.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class UserSecurity {
    @Autowired
    private LoginService service;
    
    public boolean hasUserId(Authentication authentication, int userId) {
        try {
            User user = (User) authentication.getPrincipal();
            if (userId == service.getUserByName(user.getUsername()).get().getId()) {
                return true;
            } else { 
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }
}
