package by.training.homework28.controllers;

import by.training.homework28.model.User;
import by.training.homework28.service.LoginService;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @Autowired
    LoginService service;
     
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    
    @GetMapping("/shop")
    public String shop() {
        return "shop";
    }
    
    @GetMapping("/cart")
    public String cart() {
        return "cart";
    }
    
    @GetMapping("/after-login")
    public String createSession(HttpServletRequest request, HttpSession session) {
        Optional<User> curentUser = service.getUserByName(request.getUserPrincipal().getName());
        curentUser.ifPresent((user) -> {
            int orderId = service.createOrder(user);
            session.setAttribute("terms", "true");
            session.setAttribute("name", user.getName());
            session.setAttribute("orderId", orderId);
        });
        return "redirect:/shop";
    }
}
