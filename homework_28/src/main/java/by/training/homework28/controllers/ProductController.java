package by.training.homework28.controllers;

import by.training.homework28.model.Product;
import by.training.homework28.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {
    @Autowired
    private ProductService productService;
    
    @GetMapping
    public ResponseEntity<List<Product>> getAll() {
        List<Product> products = productService.getProductList();
        if (!products.isEmpty()) {
        return ResponseEntity.ok(products);
        } else {
            return ResponseEntity.notFound().build();
        }
    }   
}
