package by.training.homework28.service;

import by.training.homework28.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.springframework.scheduling.annotation.Async;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender emailSender;
    
    @Autowired
    private SpringTemplateEngine templateEngine;
    
    @Async
    public void sendOrderList(Order order) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        
        Context context = new Context();
        context.setVariable("order", order);

        String htmlContent = this.templateEngine.process("orderList", context);
        
        helper.setTo(order.getUser().getEmail());
        helper.setText(htmlContent, true);
        helper.setSubject("Your order");
        helper.setFrom("homework28lubinets@gmail.com");
        
        emailSender.send(message);
    }
}
