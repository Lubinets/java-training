package by.training.homework28.service;

import by.training.homework28.model.Order;
import by.training.homework28.model.Product;
import by.training.homework28.repository.OrderDao;
import by.training.homework28.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import org.hibernate.Hibernate;


@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;
    
    @Autowired
    private ProductDao productDao;
    
    
    /**
     * Updates price of the order.
     * @param orderId id of the order
     * @return new price of the order
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public double updateOrderPrice(int orderId) {
        double totalPrice = 0;
        Optional<Order> orderOpt = orderDao.get(orderId);
        if (orderOpt.isPresent()) {
            Order order = orderOpt.get();
            for (Product product : order.getProducts()) {
                totalPrice += product.getProductPrice();
            }
            order.setTotalPrice(totalPrice);
        }      
        return totalPrice;
    }
    
    /**
     * Adds product to cart.
     * @param orderId id of the order
     * @param productId id of the product
     */
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void addToCart(int orderId, Product product) {
        Optional<Order> orderOpt = orderDao.get(orderId);
        if (orderOpt.isPresent()) {
            Order order = orderOpt.get();
            order.addProduct(product);
        }
    }
    
    @Transactional(isolation = Isolation.READ_COMMITTED) 
    public Optional<Order> findOrderById(int id) {
        Optional<Order> orderOpt = orderDao.get(id);
        if (orderOpt.isPresent()) {
            Hibernate.initialize(orderOpt.get().getProducts());
            return orderOpt;
        }
        return orderOpt;
    }
}
