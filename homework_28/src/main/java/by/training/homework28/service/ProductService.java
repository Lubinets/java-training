package by.training.homework28.service;

import by.training.homework28.model.Order;
import by.training.homework28.model.Product;
import by.training.homework28.repository.OrderDao;
import by.training.homework28.repository.ProductDao;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class ProductService {
    @Autowired
    private ProductDao productDao;
    
    @Autowired
    private OrderDao orderDao;
    
    public List<Product> getSelectedProducts(int orderId) {       
        List<Product> selectedProducts = new ArrayList();
        Optional<Order> orderOpt = orderDao.get(orderId);
        if (orderOpt.isPresent()) {
            Order order = orderOpt.get();
            Hibernate.initialize(order.getProducts());
            selectedProducts = order.getProducts();
        }
        return selectedProducts;
    }
    
    public List<Product> getProductList() {
        return productDao.getAll();
    }
    
    public Optional<Product> getById(int id) {
        return productDao.get(id);
    }
}
