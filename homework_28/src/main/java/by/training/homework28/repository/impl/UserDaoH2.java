package by.training.homework28.repository.impl;

import by.training.homework28.model.User;
import by.training.homework28.repository.UserDao;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Implementation of {@link UserDao} for H2 database.
 */
@Repository
public class UserDaoH2 implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Optional get(int id) {
        return Optional.ofNullable(
                sessionFactory.getCurrentSession()
                .get(User.class, id));
    }

    @Override
    public Optional getByName(String login) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from User where name = :login", User.class);
        query.setParameter("login", login);
        return Optional.ofNullable(query.list().get(0));
    }
}
