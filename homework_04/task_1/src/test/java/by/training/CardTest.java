/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

/**
 *
 * @author Admin
 */
public class CardTest {
    String expectedName;
    double expected;
    
    @Before
    public void doBefore() {
        expected = 10.245;
        expectedName = "owner";
    }
    
    @Test
    public void testAdding() {
        Card card = new Card("test");
        card.add(10.245);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test
    public void testWithdraw() {
        Card card = new Card("test", 20.245);
        card.withdraw(10.0);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
        expected = 0;
        card.withdrawAll();
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test
    public void testDisplayInOtherCurrency() {
        expected = 4.7;
        Card card = new Card("test", 10.0);
        double actual = card.displayInOtherCurrency(0.47);
        Assert.assertEquals(expected, actual, 0.00001);
    }
    
    @Test
    public void testGetOwner() {
        Card card = new Card("owner");
        Assert.assertEquals(expectedName, card.getOwner());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddWithNegativeCurrency() {
        Card card = new Card("test", 10.0);
        card.add(-20.245);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawWithNegativeCurrency() {
        Card card = new Card("test", 10.542);
        card.withdraw(-20.0);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFirstConstructorWithNull() {
        Card card = new Card(null);
        Assert.assertEquals(expectedName, card.getOwner());   
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSecondConstructorWithNull() {
        Card card = new Card(null, null);
        Assert.assertEquals(expected, card.showBalance(), 0.00001);
    }
       
    @Test(expected = IllegalArgumentException.class)
    public void testOtherCurrencyWithNull() {
        expected = 4.7;
        Card card = new Card("test", 10.0);
        double actual = card.displayInOtherCurrency(null);
        Assert.assertEquals(expected, actual, 0.00001);
    }
}
