/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

/**
 *
 * @author Admin
 */
public class AtmTest {
    Atm atm;
    double expected;
    
    @Before
    public void doBefore() {
        atm = new Atm(new Card("owner", 10.0));
        expected = 20.0;
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSelectCardWithNull() {
        atm.selectCard(null);
    }
    
    @Test
    public void testAdd() {
        atm.add(10.0);
        Assert.assertEquals(expected, atm.showBalance(), 0.000001);
    }
}
