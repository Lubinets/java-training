/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class DebitCardTest {
    @Test(expected = IllegalArgumentException.class)
    public void testDebitCardDebt() {
        double expected = -10;
        DebitCard card = new DebitCard("owner", 10.0);
        Atm atm = new Atm(card);
        atm.withdraw(20.0);
        Assert.assertEquals(expected, card.showBalance(), 0.000001);
    }
}
