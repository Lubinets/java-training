/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

/**
 *
 * @author Admin
 */
public class CreditCard extends Card {
    public CreditCard(final String owner) {
        super(owner);
    }

    public CreditCard(final String owner, final double balance) {
        super(owner, balance);
    }
}
