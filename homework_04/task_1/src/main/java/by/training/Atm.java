/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

/**
 *
 * @author Admin
 */
public class Atm {
    private Card card;

    public Atm(final Card card) {
        if (card == null) {
            throw new IllegalArgumentException();
        }
        this.card = card;
    }

    /**
     * Shows balance of card in Atm.
     * @return returns balance of card
     */
    public double showBalance() {
        return card.showBalance();
    }

    /**
     * Selects card to use in Atm.
     * @param card card to use in Atm
     */
    public void selectCard(final Card card) {
        if (card == null) {
            throw new IllegalArgumentException();
        }
        this.card = card;
    }

    /**
     * Withdraw money from balance.
     * @param currency Amount of money to withdraw
     */
    public void withdraw(final double currency) {
        card.withdraw(currency);
    }

    /**
     * Adds money to the balance.
     * @param currency Amount of money to add
     */
    public void add(final double currency) {
        card.add(currency);
    }
}
