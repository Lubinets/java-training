/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import java.util.Objects;

/**
 *
 * @author Admin
 */
public class DebitCard extends Card {
    public DebitCard(final String owner) {
        super(owner);
    }

    public DebitCard(final String owner, final double balance) {
        super(owner, balance);
    }

    /**
     * Withdraw money from balance(does not allow negative balance on card).
     * @param currency Amount of money to withdraw
     */
    public void withdraw(final Double currency) {
        if (currency < 0 || Objects.isNull(currency)) {
            throw new IllegalArgumentException();
        }
        if (balance - currency < 0) {
            throw new IllegalArgumentException("Debit card doesn't allow debt");
        }
        balance -= currency;
    }
}
