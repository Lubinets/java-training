/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

/**
 *
 * @author Admin
 */
public class SortingContext {
    private final Sorter sorter;
    
    public SortingContext(final Sorter sorter) {
        if (sorter == null) {
            throw new NullPointerException();
        }
        this.sorter = sorter;
    }

    /**
     * Sorts array in ascending order.
     * @param array sets array to sort
     */
    public void sortArray(int[] array) {
        sorter.sort(array);
    }

}
