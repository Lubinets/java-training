/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter array of ints");
        String input = in.nextLine();
        if (verifyInput(input)) {
            int[] array = Arrays.stream(input.split(" ")).mapToInt(
                    Integer::parseInt).toArray();
            System.out.println("Select sorting algorithm "
                    + "(1 - bubble sort, 2 selection sort)");
            int sortingAlgorithm = Integer.parseInt(in.nextLine());
            Sorter sorter;
            switch (sortingAlgorithm) {
                case (1) :
                    sorter = new BubbleSort();
                    break;
                case (2) :
                    sorter = new SelectionSort();
                    break;
                default :
                    throw new IllegalArgumentException();
            }
            SortingContext context = new SortingContext(sorter);
            context.sortArray(array);
            System.out.print(Arrays.toString(array));
        } else {
            throw new IllegalArgumentException("Wrong array input");
        }
    }

    private static boolean verifyInput(final String input) {
        Pattern p = Pattern.compile("([0-9]*\\s?)+");
        Matcher m = p.matcher(input);
        return m.matches();
    }
}
