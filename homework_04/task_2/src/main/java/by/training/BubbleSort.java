/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

/**
 *
 * @author Admin
 */
public class BubbleSort implements Sorter {
    /**
     * {@link Sorter#sort(int[])}.
     * @param array sets array to sort
     */
    @Override
    public void sort(final int[] array) {
        if (array == null) {
            throw new NullPointerException();
        }
        int temp;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
    }
}
