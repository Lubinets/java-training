/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

/**
 *
 * @author Admin
 */
public interface Sorter {
    /**
     * Sorts array in ascending order.
     * @param array sets array to sort
     */
    void sort(int[] array);
}
