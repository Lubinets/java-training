/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

/**
 *
 * @author Admin
 */
public class SelectionSort implements Sorter {
    /**
     * {@link Sorter#sort(int[])}.
     * @param array sets array to sort
     */
    @Override
    public void sort(final int[] array) {
        if (array == null) {
            throw new NullPointerException();
        }
        int min;
        int temp;
        for (int i = 0; i < array.length - 1; i++) {
            min = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            temp = array[min];
            array[min] = array[i];
            array[i] = temp;
        }
    }
}
