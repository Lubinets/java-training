/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

/**
 *
 * @author Admin
 */
public class TestSorter {
    int[] actual;
    int[] expected;
    
    @Before
    public void doBefore() {
        actual = new int[] {2, 7, 1, 4, 5, 8};
        expected = new int[] {1, 2, 4, 5, 7, 8};
    }
    
    @Test
    public void testBubbleSort() {
        BubbleSort sorter = new BubbleSort();
        SortingContext context = new SortingContext (sorter);
        context.sortArray(actual);
        Assert.assertArrayEquals(expected, actual);
    }
    
    @Test
    public void testSelectionSort() {
        SelectionSort sorter = new SelectionSort();
        SortingContext context = new SortingContext (sorter);
        context.sortArray(actual);
        Assert.assertArrayEquals(expected, actual);
    }
}
