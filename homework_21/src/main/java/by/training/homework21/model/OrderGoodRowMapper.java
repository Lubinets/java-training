package by.training.homework21.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class OrderGoodRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        OrderGood orderGood = new OrderGood();
        orderGood.setId(rs.getInt("ID"));
        orderGood.setOrderId(rs.getInt("order_id"));
        orderGood.setGoodId(rs.getInt("good_id"));
        return orderGood;
    }
}
