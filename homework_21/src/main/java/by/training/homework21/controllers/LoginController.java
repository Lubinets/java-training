package by.training.homework21.controllers;

import by.training.homework21.model.Order;
import by.training.homework21.model.User;
import by.training.homework21.repository.OrderDao;
import by.training.homework21.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.ui.Model;

@Controller
public class LoginController {
    @Autowired
    UserDao userDao;
    
    @Autowired
    OrderDao orderDao;
    
    @GetMapping(value = "/after-login")
    public String createSession(HttpServletRequest request, HttpSession session) {
        Optional<User> curentUser = userDao.getByName(request.getUserPrincipal().getName());
        curentUser.ifPresent((user) -> {
            Order order = new Order(0, user.getId(), 0);
            int orderId = orderDao.save(order);
            session.setAttribute("terms", "true");
            session.setAttribute("name", user.getName());
            session.setAttribute("userId", user.getId());
            session.setAttribute("orderId", orderId);
        });
        return "redirect:/shop";
    }
    
    @GetMapping("/login")
    public Model getLoginPage(Model model) {
        return model;
    }
}
