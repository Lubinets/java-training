package by.training.homework21.controllers;

import by.training.homework21.model.Product;
import by.training.homework21.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import javax.servlet.http.HttpSession;

@Controller
public class CartController {
    @Autowired
    OrderService service;
    
    @GetMapping(value = "/cart")
    public Model getUserCart(Model model, HttpSession session) {
        int orderId = (Integer) session.getAttribute("orderId");
        List<Product> selectedProducts = service.getAllOrderedProductsById(orderId);
        if (!selectedProducts.isEmpty()) {
            model.addAttribute("cost", service.getTotalPrice());
            model.addAttribute("emptyCart", "false");
        } else {
            model.addAttribute("emptyCart", "true");
        }
        
        model.addAttribute("selectedItems", selectedProducts);
        return model;
    }
}
