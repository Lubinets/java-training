package by.training.homework21.service;

import by.training.homework21.model.OrderGood;
import by.training.homework21.model.Product;
import by.training.homework21.repository.OrderDao;
import by.training.homework21.repository.OrderGoodDao;
import by.training.homework21.repository.ProductDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;
    
    @Autowired
    private OrderGoodDao orderGoodDao;
    
    @Autowired
    private ProductDao productDao;
    
    private double totalPrice = 0;
    
    /**
     * Returns list of products from the selected order.
     * @param orderId id of the order
     * @return list of products
     */
    public List<Product> getAllOrderedProductsById(int orderId) {
        ArrayList<Product> selectedProducts = new ArrayList();
        List<OrderGood> orderGoods = orderGoodDao.getByOrderId(orderId);
        if (!orderGoods.isEmpty()) {
            for (OrderGood good : orderGoods) {
                Optional<Product> productOpt = productDao.get(good.getGoodId());
                if (productOpt.isPresent()) {
                    Product product = productOpt.get();
                    selectedProducts.add(product);
                    totalPrice += product.getProductPrice();
                }
            }
            orderDao.updateTotalPrice(orderId, totalPrice);
        }
       
        return selectedProducts;
    }
    
    /**
     * @param orderDao the orderDao to set
     */
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * @param orderGoodDao the orderGoodDao to set
     */
    public void setOrderGoodDao(OrderGoodDao orderGoodDao) {
        this.orderGoodDao = orderGoodDao;
    }

    /**
     * @param productDao the productDao to set
     */
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    /**
     * Returns total price of the product selected by {@link getAllOrderedProductsById()}.
     * Returns 0 if {@link getAllOrderedProductsById()} has not been use yet.
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }
}
