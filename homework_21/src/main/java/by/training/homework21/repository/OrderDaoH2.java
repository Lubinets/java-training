package by.training.homework21.repository;

import by.training.homework21.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;


/**
 * Implementation of {@link OrderDao} for H2 database. 
 */
@Repository
public class OrderDaoH2 implements OrderDao {
    private final String UPDATE_PRICE = "UPDATE Orders SET total_price = ? WHERE ID = ?";
    private final String SAVE = "INSERT INTO Orders (user_id, total_price) VALUES(? , ?)";
    
    private JdbcTemplate connection;
    
    @Autowired
    public OrderDaoH2(JdbcTemplate template) {
        connection = template;
    }

    @Override
    public int save(Order order) {
            GeneratedKeyHolder holder = new GeneratedKeyHolder();
            connection.update((Connection con) -> {
                PreparedStatement st = con.prepareStatement(SAVE, Statement.RETURN_GENERATED_KEYS);
                st.setInt(1, order.getUserId());
                st.setInt(2, 0);
                return st;
            }, holder);
            int orderId = holder.getKey().intValue();
            return orderId;
    }

    @Override
    public void updateTotalPrice(int id, double totalPrice) {
        connection.update(UPDATE_PRICE, totalPrice, id);
    }
}
