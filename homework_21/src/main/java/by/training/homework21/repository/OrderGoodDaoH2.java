package by.training.homework21.repository;

import by.training.homework21.model.OrderGood;
import by.training.homework21.model.OrderGoodRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

/**
 * Implementation of {@link OrderGoodDao} for H2 database.
 * @author Admin
 */
@Repository
public class OrderGoodDaoH2 implements OrderGoodDao {
    private final String SAVE = "INSERT INTO Order_Good (order_id, good_id) VALUES (? , ?)";
    private final String GET_BY_ORDER_ID = "SELECT * FROM Order_Good WHERE order_Id = ?";
    
    private JdbcTemplate connection;
    
    @Autowired
    public OrderGoodDaoH2(JdbcTemplate template) {
        connection = template;
    }
    
    @Override
    public void save(OrderGood orderGood) {
        connection.update((Connection con) -> {
                PreparedStatement st = con.prepareStatement(SAVE);
                st.setInt(1, orderGood.getOrderId());
                st.setInt(2, orderGood.getGoodId());
                return st;
            });
    }
    
    @Override
    public List<OrderGood> getByOrderId(int orderId) {
            List<OrderGood> resultList = connection.query(GET_BY_ORDER_ID,
                    new OrderGoodRowMapper(), orderId);
            return resultList;
    }
   
}
