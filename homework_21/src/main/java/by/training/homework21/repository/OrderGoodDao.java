package by.training.homework21.repository;

import by.training.homework21.model.OrderGood;

import java.util.List;

/**
 * Dao to work with Order_Good table.
 */
public interface OrderGoodDao {   
    /**
     * Saves order good into database.
     * @param orderGood Order good to ave
     */
    public void save(OrderGood orderGood);
    
    /**
     * Gets list of all order goods for specific order.
     * @param orderId id of the order
     * @return return list of order goods
     */
    public List<OrderGood> getByOrderId(int orderId);
    
}
