package by.training.homework_06;


import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class StringParserTest {
    @Test
    public void testParse() {
        String input = "Once upon a time a Wolf was "
                + "lapping at a spring on a hillside, when, looking up, what "
                + "should he see but a Lamb just beginning to drink a "
                + "little lower down.";
        String expected = "A\n" +
                "a - 6\n" +
                "at - 1\n" +
                "B\n" +
                "beginning - 1\n" +
                "but - 1\n" +
                "D\n" +
                "down - 1\n" +
                "drink - 1\n" +
                "H\n" +
                "he - 1\n" +
                "hillside - 1\n" +
                "J\n" +
                "just - 1\n" +
                "L\n" +
                "lamb - 1\n" +
                "lapping - 1\n" +
                "little - 1\n" +
                "looking - 1\n" +
                "lower - 1\n" +
                "O\n" +
                "on - 1\n" +
                "once - 1\n" +
                "S\n" +
                "see - 1\n" +
                "should - 1\n" +
                "spring - 1\n" +
                "T\n" +
                "time - 1\n" +
                "to - 1\n" +
                "U\n" +
                "up - 1\n" +
                "upon - 1\n" +
                "W\n" +
                "was - 1\n" +
                "what - 1\n" +
                "when - 1\n" +
                "wolf - 1\n";
        StringParser parser = new StringParser(input);
        parser.parse();
        Assert.assertEquals(expected, parser.toString());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testParseWithEmptyString() {
        StringParser parser = new StringParser("");
    }
}
