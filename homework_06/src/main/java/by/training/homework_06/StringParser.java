package by.training.homework_06;

import java.util.Map;
import java.util.TreeMap;

/**
 * Used to count the number of words in the text.
 */
public class StringParser {
    private String input;
    private Map<String, Integer> wordsNumber;

    public StringParser(final String input) {
        if (input.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.input = input;
    }

    /**
     * Parses text to find words and their number in the text. 
     * Use {@link #toString() ()} to return result.
     */
    public void parse() {
        wordsNumber = new TreeMap<>();
        input = input.replaceAll("[\\/.,!?]", "").toLowerCase();
        String[] tokens = input.split("\\s");
        for (String s : tokens) {
            if (wordsNumber.containsKey(s)) {
                wordsNumber.replace(s, wordsNumber.get(s) + 1);
            } else {
                wordsNumber.put(s, 1);
            }
        }
    }

    /**
     * Transforms result of the class work to string.
     * @return returns string result of {@link #parse()} work.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        wordsNumber.entrySet().forEach((item) -> {
            String word = item.getKey();
            Integer number = item.getValue();
            String firstLetter = word.toUpperCase().charAt(0) + "\n";
            if (!builder.toString().contains(firstLetter)) {
                builder.append(firstLetter);
            }
            builder.append(word);
            builder.append(" - ");
            builder.append(number);
            builder.append("\n");
        });
        return builder.toString();
    }
}
