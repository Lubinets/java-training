package by.training.homework_06;

import static java.lang.System.in;
import java.util.Scanner;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Enter input string");
        Scanner scan = new Scanner(in);
        String input = scan.nextLine();
        StringParser parser = new StringParser(input);
        parser.parse();
        System.out.println(parser.toString());
    }
}
