package by.training.homework15.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for choosing items to buy from the online shop.
 */
@WebServlet(name = "ShopServlet", urlPatterns = {"/Shop"})
public class ShopServlet extends HttpServlet {
    Map<String, Double> goodsList;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            initGoods();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");        
            out.println("</head>");
            out.println("<body>");
            out.println("<form method='POST' action='Cart' style='margin: 0 auto; "
                    + "text-align: center'>");
            out.println("<h1>Hello " + request.getParameter("name") + "!</h1>");
            out.println("<select style='width: 25%' name='selectedItems[]' multiple='multiple'>");
            goodsList.forEach(
                    (key, value) -> out.println("<option>" + key + " " + value + "$ </option>"));
            out.println("</select> </br>");
            out.println("<input name='name' type=hidden value='"
                    + request.getParameter("name") + "'>");
            out.println("<input type=submit value='Sumbit'>");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    private void initGoods() {
        goodsList = new HashMap();
        goodsList.put("Book", 5.5);
        goodsList.put("Mobile phone", 10.0);
        goodsList.put("First test obj", 4.5);
        goodsList.put("Second Test obj", 11.0);
        goodsList.put("Third Test obj", 6.0);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    } // </editor-fold>
}
