package by.training.homework16.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet for creating web-page with users cart in the online shop.
 */
@WebServlet(name = "CartServlet", urlPatterns = {"/Cart"})
public class CartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Welcome to online shop</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<div style='margin: 0 auto; text-align: center'>");
            
            if (Objects.nonNull(request.getSession().getAttribute("selectedItem"))) {
            out.println("<h1> Dear " + request.getSession().getAttribute("name")
                    + ", your order</h1>");            
                ArrayList<String> cartList = (ArrayList<String>) 
                        request.getSession().getAttribute("selectedItem");
                double cost = 0;
                for (String s : cartList) {
                    out.println("<h2>" + s + "</h2>");
                    cost += Double.valueOf(s.replaceAll("[^\\d\\.]", ""));
                }
                out.println("<span>Total: " + cost + "$</span>");
            } else {
                out.println("<h1>Your cart is empty</h1>");
            }
            
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
            request.getSession().invalidate();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    } // </editor-fold>
}
