package by.training.homework16.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet for choosing items to buy from the online shop.
 */
@WebServlet(name = "ShopServlet", urlPatterns = {"/Shop"})
public class ShopServlet extends HttpServlet {
    Map<String, Double> goodsList;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            initGoods();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("</head>");
            out.println("<body>");
            out.println("<div style='margin: 0 auto; text-align: center'>");
            HttpSession session = request.getSession();
            out.println("<h1>Hello " + session.getAttribute("name") + "!</h1>");
            out.println("<div> You have already chosen:</div>");
            out.println("<ol style='list-style-position: inside; padding-left: 0;'>");
            if (Objects.nonNull(session.getAttribute("selectedItem"))) {
                ArrayList<String> itemList = (ArrayList<String>) 
                        session.getAttribute("selectedItem");
                for (String item : itemList) {
                    out.println("<li>" + item);
                }
            }
            out.println("</ol>");
            out.println("<form method='GET' action='AddToCartServlet'>");
            out.println("<select required style='width: 25%' name='selectedItem'>");
            goodsList.forEach(
                    (key, value) -> out.println("<option>" + key + " " + value + "$ </option>"));
            out.println("</select> </br>");
            out.println("<input type=submit value='Add item'> </br>");
            out.println("</form>");
            out.println("<input onclick=window.location.href='Cart' type='button' value='Submit'>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    private void initGoods() {
        goodsList = new HashMap();
        goodsList.put("Book", 5.5);
        goodsList.put("Mobile phone", 10.0);
        goodsList.put("First test obj", 4.5);
        goodsList.put("Second Test obj", 11.0);
        goodsList.put("Third Test obj", 6.0);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    } // </editor-fold>
}
