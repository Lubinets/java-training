package by.training.homework16.servlets;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filter for checking if user agreed with terms of service.
 */
@WebFilter(filterName = "TermsOfServiceFilter", urlPatterns = {"/Shop", "/Cart"}, dispatcherTypes = {DispatcherType.REQUEST})
public class TermsOfServiceFilter implements Filter {
    
    private static final boolean debug = true;

    private FilterConfig filterConfig = null;
    
    public TermsOfServiceFilter() {
    }      

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("TermsOfServiceFilter:doFilter()");
        }
        
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpSession session = servletRequest.getSession();
        if (Objects.nonNull(request.getParameter("terms"))) {
            session.setAttribute("terms", "true");
            session.setAttribute("name", request.getParameter("name"));
            log("Session is created");
        }
        log("Terms of service = " + session.getAttribute("terms"));
        if (Objects.isNull(session.getAttribute("terms"))) {
            log("User has not agreed with terms of service");
            HttpServletResponse servletResponse = (HttpServletResponse) response;
            servletResponse.sendRedirect("TermsOfServiceServlet");
        }
        
        try {
            chain.doFilter(request, response);
        } catch (Throwable exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter.
     */
    @Override
    public void destroy() {        
    }

    /**
     * Init method for this filter.
     */
    @Override
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {                
                log("TermsOfServiceFilter:Initializing filter");
            }
        }
    }
    
    /**
     * Used to logging to console.
     * @param msg message to log
     */
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);        
    }
    
}
