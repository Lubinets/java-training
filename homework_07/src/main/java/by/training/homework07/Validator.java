package by.training.homework07;

/**
 * Interface for validating input.
 * @param <T> type of element to validate
 */
public interface Validator<T> { 
    /**
     * Validates input. Should throw 
     *{@link ValidationFailedException} if failed.
     * @param input input for validation 
     */
    public void validate(T input);
}
