package by.training.homework07;

/**
 * Enumeration to choose specific validator by input class.
 */
public enum ValidatorFactory {
    /**
     * String validator.
     */
    String { 
        public StringValidator getValidator() {
            return new StringValidator(); 
        }
    },
    
    /**
     * Integer validator.
     */
    Integer { 
        public IntegerValidator getValidator() {
            return new IntegerValidator();
        }
    };
    
    /**
     * Implment this method in enum values to return specific validator.
     * @return returns specific validator
     */
    public abstract Validator getValidator();
}
