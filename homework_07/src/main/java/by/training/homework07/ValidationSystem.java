package by.training.homework07;

/**
 * Used for validating input if input type has specefic validator that 
 * implements {@link Validator} created and registered in 
 * {@link ValidationFactory}.
 */
public class ValidationSystem {
    
    /**
     * Validates input depending on its type.
     * Throws ValidationFailedException if not valid.
     * @param input input for validating
     */
    public static void validate(Object input) {
        if (input == null) {
            throw new IllegalArgumentException();
        }
        Validator validator = ValidatorFactory.valueOf(
                input.getClass().getSimpleName()).getValidator();
        validator.validate(input);
    }
}
