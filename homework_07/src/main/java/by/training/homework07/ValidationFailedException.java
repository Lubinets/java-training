package by.training.homework07;

/**
 * Thrown by classes that implement {@link Validator} interface if 
 * validation has failed.
 */
public class ValidationFailedException extends RuntimeException { }
