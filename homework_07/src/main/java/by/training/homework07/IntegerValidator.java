package by.training.homework07;

/**
 * Validates integer input.
 * @param <T> type of element to validate
 */
public class IntegerValidator<T extends Integer> implements Validator<T> {   
    /**
     * Checks if integer input is in [1-10] range.
     * Throws {@link ValidationFailedException} if not valid.
     * @param input input number
     */
    @Override
    public void validate(Integer input) {
        if (input < 1 || input > 10) {
            throw new ValidationFailedException();
        }
    }
}
