package by.training.homework07;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates string input.
 * @param <T> type of element to validate
 */
public class StringValidator<T extends String> implements Validator<T> {
    /**
     * Checks if string input starts with upper case letter. Throws 
     * {@link ValidationFailedException} if not valid.
     * @param input string input
     */
    @Override
    public void validate(String input) {
        Pattern pattern = Pattern.compile("^[A-Z]");
        Matcher matcher = pattern.matcher(input);
        if (!matcher.find()) {
            throw new ValidationFailedException();
        }
    }
}
