package by.training.homework14;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Class for connecting and working with https://jsonplaceholder.typicode.com
 * with usage of HttpClient.
 */
public class HttpClientMethod implements HttpMethods {
    private static final Logger log = Logger.getLogger(ConnectionMethod.class);
    
    @Override
    public Post post(int id) throws IllegalArgumentException {
        if (id <= 0) {
            String message = this.getClass().getCanonicalName()
                    + " - ID of post can't be zero or negative";
            throw new IllegalArgumentException(message);
        }
        Post post = new Post(id);
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("https://jsonplaceholder.typicode.com/posts");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(post);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse response = client.execute(httpPost);
            StatusLine status = response.getStatusLine();
            post = objectMapper.readValue(response.getEntity().getContent(), Post.class);
            log.info("POST Response Code :  " + status.getStatusCode());
            log.info("POST Response Message : " + status.getReasonPhrase());
            log.info(post.toString());
            return post;
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        return new Post();
    }
    
    @Override
    public Post get(int id) throws IllegalArgumentException {
        if (id <= 0) {
            String message = this.getClass().getCanonicalName()
                    + " - ID of post can't be zero or negative";
            throw new IllegalArgumentException(message);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("https://jsonplaceholder.typicode.com/posts/" + id);
        try {
            HttpResponse response = client.execute(request);
            Post post = objectMapper.readValue(response.getEntity().getContent(), Post.class);
            log.info(post.toString());
            return post;
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        return new Post();
    }
}
