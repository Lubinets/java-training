package by.training.homework14;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class for connecting and working with https://jsonplaceholder.typicode.com
 * with usage of HttpURLConnection.
 */
public class ConnectionMethod implements HttpMethods {
    private static final Logger log = Logger.getLogger(ConnectionMethod.class);
    
    @Override
    public Post post(int id) throws IllegalArgumentException {
        if (id <= 0) {
            String message = this.getClass().getCanonicalName()
                    + " - ID of post can't be zero or negative";
            throw new IllegalArgumentException(message);
        }
        Post post = new Post(id);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            URL url = new URL("https://jsonplaceholder.typicode.com/posts");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);
            byte[] json = objectMapper.writeValueAsBytes(post);
            OutputStream os = connection.getOutputStream();
            os.write(json);
            os.flush();
            os.close();
            post = objectMapper.readValue(connection.getInputStream(), Post.class);
            int responseCode = connection.getResponseCode();
            log.debug("POST Response Code :  " + responseCode);
            log.debug("POST Response Message : " + connection.getResponseMessage());
            log.info(post.toString());
            return post;
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        return new Post();
    }
    
    @Override
    public Post get(int id) throws IllegalArgumentException {
        if (id <= 0) {
            String message = this.getClass().getCanonicalName()
                    + " - ID of post can't be zero or negative";
            throw new IllegalArgumentException(message);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            URL url = new URL("https://jsonplaceholder.typicode.com/posts/" + id);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoInput(true);
            Post post = objectMapper.readValue(connection.getInputStream(), Post.class);
            log.info(post.toString());
            return post;
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        return new Post();
    }
}
