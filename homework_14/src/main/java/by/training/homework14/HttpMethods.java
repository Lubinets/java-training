package by.training.homework14;

/**
 * Interface for creating implementation of POST and GET methods to connect to
 * https://jsonplaceholder.typicode.com.
 */
public interface HttpMethods {
    /**
     * Implementation of method POST. Tries to create post with set id at
     * https://jsonplaceholder.typicode.com/posts
     * @param id id of the post to create
     * @return returns created post or empty post if unsuccessful
     */
    public Post post(int id);
    
    /**
     * Implementation of method GET. Tries to get post with set id from
     * https://jsonplaceholder.typicode.com/posts
     * @param id id of the post to get
     * @return returns post with set id or empty post if unsuccessful
     */
    public Post get(int id);
}
