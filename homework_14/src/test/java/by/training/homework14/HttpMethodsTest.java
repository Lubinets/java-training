/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training.homework14;

import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import static org.junit.Assert.*;
import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.AdditionalMatchers.leq;
import static org.mockito.Mockito.*;

/**
 *
 * @author Admin
 */
@RunWith(Parameterized.class)
public class HttpMethodsTest {
    @Mock
    HttpMethods method;
    
    Post expected;
    
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new HttpClientMethod()},
                {new ConnectionMethod()}
        });
    }

    public HttpMethodsTest(HttpMethods method) {
        this.method = mock(method.getClass());
    }
    
    @Test
    public void testPost() {
        when(method.post(gt(0))).thenReturn(new Post(101));
        assertEquals(new Post(101), method.post(3));
    }
    
    @Test
    public void testGet() {
        when(method.get(gt(0))).thenReturn(expected);
        assertEquals(expected, method.get(2));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testPostWithNegative() {
        when(method.post(leq(0))).thenThrow(IllegalArgumentException.class);
        assertEquals(expected, method.post(-1));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetWithNegative() {
        when(method.get(leq(0))).thenThrow(IllegalArgumentException.class);
        assertEquals(expected, method.get(-3));
    }
}
