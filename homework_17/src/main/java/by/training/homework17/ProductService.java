package by.training.homework17;

import java.util.ArrayList;
import java.util.Objects;


public class ProductService {  
    private static ArrayList<Product> goodsList;
    
    /**
     * Getter for goodsList.
     * @return returns array of products.
     */
    public static ArrayList<Product> getGoods() {
        if (Objects.isNull(goodsList)) {
            initGoods();
        }
        return goodsList;
    }
    
    /**
     * Initializes array of Products.
     */
    private static void initGoods() {
        goodsList = new ArrayList();
        goodsList.add(new Product("Book", 5.5));
        goodsList.add(new Product("Mobile phone", 10.0));
        goodsList.add(new Product("First test obj", 4.5));
        goodsList.add(new Product("Second Test obj", 11.0));
        goodsList.add(new Product("Third Test obj", 6.0));
    }
}
