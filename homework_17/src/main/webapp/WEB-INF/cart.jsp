<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to online shop</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="block-middle">
            <c:choose>
                <c:when test="${emptyCart == 'true'}">
                    <h1>Your cart is empty</h1>
                </c:when>
                <c:otherwise>
                   <h1> Dear ${name} your order</h1>
                   <c:forEach items="${selectedItem}" var="selected">
                        <h2><c:out value="${selected}"/></h2>
                   </c:forEach>
                   <span>Total: <c:out value="${cost}"/>$</span>
                </c:otherwise>
            </c:choose>
        </div>
        <% session.invalidate();%>
    </body>
</html>
