package by.training.homework08;

/**
 * Throw wnen trying to insert more elements then {@link LimitedList} capacity
 * allows.
 */
public class ListOutOfBoundsException extends RuntimeException {

}
