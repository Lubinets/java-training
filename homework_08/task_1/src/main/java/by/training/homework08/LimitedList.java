package by.training.homework08;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


/**
 * Collection with limited capacity.
 * @param <E> type of elements to store
 */
public class LimitedList<E> implements List<E> {
    private Object[] elements;
    private int size;
    private int capacity;
    
    public LimitedList(int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException();
        }
        elements = new Object[capacity];
        this.capacity = capacity;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size > 0;          
    }

    @Override
    public boolean contains(Object object) {
        boolean contains = false;
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(object)) {
                contains = true;
            }
        }
        return contains;
    }

    @Override
    public boolean add(E element) {
        if (size == capacity) {
            throw new ListOutOfBoundsException();
        }
        elements[size] = element;
        size++;
        return true;
    }
    
    @Override
    public void add(int index, E element) {
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        if (size == capacity) {
            throw new ListOutOfBoundsException();
        }
        Object prevElement = elements[index];
        elements[index] = element;
        for (int i = index + 1; i < size + 1; i++) {
            Object temp = prevElement;
            prevElement = elements[i];
            elements[i] = temp;
        }
        size++;
    }

    @Override
    public boolean remove(Object object) {
        boolean isRemoved = false;
        int removedIndex = 0;
        
        for (int i = 0; i < size; i++) {
            if (object.equals(elements[i])) {
                isRemoved = true;
                removedIndex = i;
                break;
            }
        }
        if (isRemoved) {
            for (int i = removedIndex; i < size - 1; i++) {
                elements[i] = elements[i + 1];
            }
            elements[size - 1] = null;
            size--;
        }
        return isRemoved;
    }
    
    @Override
    public E remove(int index) {
        Object removedElement = elements[index];
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        elements[size - 1] = null;
        size--;
        return (E) removedElement;
    }

    @Override
    public void clear() {
        elements = new Object[capacity];
        size = 0;
    }

    @Override
    public E set(int index, E element) {
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        Object prevElement = elements[index];
        elements[index] = element;
        return (E) prevElement;
    }

    @Override
    public int indexOf(Object object) {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(object)) {
                index = i;
            }
        }
        return index;
    }
    
    @Override
    public E get(int index) {
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        return (E) elements[index];
    }
    
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof LimitedList)) {
            return false;
        }
        if (hashCode() == object.hashCode()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Arrays.deepHashCode(this.elements);
        hash = 79 * hash + this.size;
        hash = 79 * hash + this.capacity;
        return hash;
    }
       
    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterator iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator listIterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator listIterator(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
