/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training.homework08;

import by.training.homework08.ListOutOfBoundsException;
import by.training.homework08.LimitedList;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class LimitedListTest {
    LimitedList expected;
    
    public LimitedListTest() {
        expected = new LimitedList<Integer>(6);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
    }
    
    @Test
    public void testAdd() {       
        LimitedList<Integer> actual = new LimitedList(6);
        actual.add(2);
        actual.add(4);
        actual.add(5);
        actual.add(1, 3);
        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testRemove() {       
        LimitedList<Integer> actual = new LimitedList(6);
        actual.add(2);
        actual.add(3);
        actual.add(4);
        actual.add(5);
        actual.add(6);
        actual.add(7);
        actual.remove(4);
        actual.remove(new Integer(7));
        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testContains() {
        boolean actual = expected.contains(5);
        Assert.assertEquals(true, actual);
    }
    
    @Test
    public void testIndexOf() {
        int expectedIndex = expected.indexOf(5);
        Assert.assertEquals(3, expectedIndex);
    }
    
    @Test
    public void testGet() {
        Assert.assertEquals(3, expected.get(1));
    }
    
    @Test
    public void testSet() {
        LimitedList<Integer> actual = new LimitedList(6);
        actual.add(2);
        actual.add(4);
        actual.add(4);
        actual.add(5);
        actual.set(1, 3);
        Assert.assertEquals(expected, actual);
    }
    
    @Test(expected = ListOutOfBoundsException.class)
    public void testAddMoreThenCapacity() {
        expected.add(6);
        expected.add(7);
        expected.add(8);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testWithNegativeCapacity() {
        LimitedList<Integer> actual = new LimitedList(-2);
    }
    
    @Test
    public void testClear() {
        expected.clear();
        Assert.assertEquals(new LimitedList(6), expected);
    }
}
