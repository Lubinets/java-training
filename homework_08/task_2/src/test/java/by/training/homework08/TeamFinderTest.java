package by.training.homework08;

import by.training.homework08.Skills;
import by.training.homework08.BuildersTeam;
import by.training.homework08.Worker;
import by.training.homework08.TeamFinder;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class TeamFinderTest {
    private TeamFinder finder;
    
    public TeamFinderTest() {
        finder = new TeamFinder();
        finder.addRequiredSkill(Skills.PAINTING, 2);
        finder.addRequiredSkill(Skills.ENGINEERING, 1);
        finder.addRequiredSkill(Skills.BRICKLAYING, 1);
        
        BuildersTeam team1 = new BuildersTeam(1);
        initTeam1(team1);
        BuildersTeam team2 = new BuildersTeam(2);
        initTeam2(team2);
        BuildersTeam team3 = new BuildersTeam(3);
        initTeam3(team3);
        finder.addTeam(team1);
        finder.addTeam(team2);
        finder.addTeam(team3);
    }
    
    private void initTeam1(BuildersTeam team) {
        ArrayList<Skills> skillList1 = new ArrayList();
        skillList1.add(Skills.CARPENTRY);
        skillList1.add(Skills.ENGINEERING);
        ArrayList<Skills> skillList2 = new ArrayList();
        skillList2.add(Skills.PAINTING);
        skillList2.add(Skills.BRICKLAYING);
        ArrayList<Skills> skillList3 = new ArrayList();
        skillList3.add(Skills.PAINTING);
        skillList3.add(Skills.ENGINEERING);
        team.addWorker(new Worker(400, skillList1));
        team.addWorker(new Worker(500, skillList2));
        team.addWorker(new Worker(500, skillList3));
    }
    
    private void initTeam2(BuildersTeam team) {
        ArrayList<Skills> skillList1 = new ArrayList();
        skillList1.add(Skills.PAINTING);
        skillList1.add(Skills.ENGINEERING);
        ArrayList<Skills> skillList2 = new ArrayList();
        skillList2.add(Skills.PAINTING);
        skillList2.add(Skills.BRICKLAYING);
        team.addWorker(new Worker(500, skillList1));
        team.addWorker(new Worker(500, skillList2));
    }
    
    private void initTeam3(BuildersTeam team) {
        ArrayList<Skills> skillList1 = new ArrayList();
        skillList1.add(Skills.CARPENTRY);
        skillList1.add(Skills.ENGINEERING);
        ArrayList<Skills> skillList2 = new ArrayList();
        skillList2.add(Skills.PAINTING);
        skillList2.add(Skills.BRICKLAYING);
        team.addWorker(new Worker(400, skillList1));
        team.addWorker(new Worker(500, skillList2));
    }
    
    @Test
    public void testTeamFinder() {
        BuildersTeam expected = new BuildersTeam(2);
        initTeam2(expected);
        Assert.assertEquals(expected, finder.findTeam());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testFinderWithNoTeams() {
        TeamFinder finder = new TeamFinder();
        finder.findTeam();
    }
}
