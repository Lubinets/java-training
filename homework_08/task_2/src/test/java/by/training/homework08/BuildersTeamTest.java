/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training.homework08;


import by.training.homework08.BuildersTeam;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class BuildersTeamTest {
    private BuildersTeam team;
    
    public BuildersTeamTest() {
        team = new BuildersTeam(0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddWorkerWithNull() {
        BuildersTeam team = new BuildersTeam(0);
        team.addWorker(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSearchForSkillWithNull() {
        BuildersTeam team = new BuildersTeam(0);
        team.searchForSkill(null);
    }
    
}
