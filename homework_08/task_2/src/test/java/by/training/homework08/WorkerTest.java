package by.training.homework08;

import by.training.homework08.Worker;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class WorkerTest {
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNegativeSalary() {
        Worker worker = new Worker(-200, new ArrayList());
    }
}
