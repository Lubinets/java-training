package by.training.homework08;

/**
 * Enumeration of skills used at construction works.
 */
public enum Skills {
    BRICKLAYING,
    CARPENTRY,
    CEMENTPOURING,
    ENGINEERING,
    PAINTING;
}
