package by.training.homework08;

import java.util.ArrayList;

/**
 * Abstract representation of construction team.
 */
public class BuildersTeam {
    private int groupId;
    private ArrayList<Worker> workers;
    
    public BuildersTeam(int groupId) {
        this.groupId = groupId;
        workers = new ArrayList();
    }
    
    /**
     * Adds new worker to the team.
     * @param worker worker to add
     */
    public void addWorker(Worker worker) {
        if (worker == null) {
            throw new IllegalArgumentException();
        }
        workers.add(worker);
    }
            
    /**
     * Gets overall price of the team.
     * @return returns price of the team
     */
    public int getOverallPrice() {
        int overallPrice = 0;
        for (Worker worker : workers) {
            overallPrice += worker.getSalary();
        }
        return overallPrice;
    }
    
    /**
     * Searches if team workers have specific skill.
     * @param skill skill to search
     * @return returns amount of workers with selected skill
     */
    public int searchForSkill(Skills skill) {
        if (skill == null) {
            throw new IllegalArgumentException();
        }
        int workersWithSkill = 0;
        for (Worker worker : workers) {
            if (worker.getSkillList().contains(skill)) {
                workersWithSkill++;
            }
        }
        return workersWithSkill;
    }
    
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof BuildersTeam)) {
            return false;
        }
        return hashCode() == object.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.groupId;
        return hash;
    }
}
