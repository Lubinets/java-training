package by.training.homework08;

import java.util.ArrayList;

/**
 * Abstract representation of a builder.
 */
public class Worker {
    private final int salary;
    private final ArrayList<Skills> buildingSkills;
    
    public Worker(final int salary, ArrayList<Skills> buildingSkills) {
        if (salary < 0 || buildingSkills.isEmpty() || buildingSkills == null) {
            throw new IllegalArgumentException();
        }
        this.salary = salary;
        this.buildingSkills = buildingSkills;
    }
    
    /**
     * Gets price of the worker.
     * @return returns price of the worker
     */
    public int getSalary() {
        return salary;
    }
    
    /**
     * Gets list of the workers' building skills.
     * @return ArrayList with workers skills
     */
    public ArrayList<Skills> getSkillList() {
        return buildingSkills;
    }
}
