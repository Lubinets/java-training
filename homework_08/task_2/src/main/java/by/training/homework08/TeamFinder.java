package by.training.homework08;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Selects builders team from the list, which have all required skills and cost
 * the least.
 */
public class TeamFinder {
    private ArrayList<BuildersTeam> buildersTeams;
    private ArrayList<BuildersTeam> suitableTeams;
    private HashMap<Skills, Integer> requiredSkills;
    
    public TeamFinder() {
        buildersTeams = new ArrayList();
        suitableTeams = new ArrayList();
        requiredSkills = new HashMap();
    }
    
    /**
     * Selects builders team from the list, which have all required 
     * skills and cost the least. Add required skills with 
     * {@link #addRequiredSkill(by.training.homework_08.Skills, int) } and
     * builders teams {@link #addTeam(by.training.homework_08.BuildersTeam) }
     * before using this method.
     * @return returns suited builders team from the list. 
     * Returns empty {@link BuildersTeam} with id = 0 if not suitable teams
     * found.
     */
    public BuildersTeam findTeam() {
        if (buildersTeams.isEmpty() || requiredSkills.isEmpty()) {
            throw new IllegalArgumentException("No teams or no required skills "
                    + "added to the finder finder");
        }
        findSuitableTeams();
        if (suitableTeams.isEmpty()) {
            return new BuildersTeam(0);
        }
        suitableTeams.sort((o1, o2) -> {
            return o1.getOverallPrice() > o2.getOverallPrice() 
                    ? 1 : o2.getOverallPrice() > o1.getOverallPrice()
                    ? -1 : 0;
        });
        return suitableTeams.get(0);
    }
    
    /**
     * Add skill requirement for the finder.
     * @param skill required skill from {@link Skills}
     * @param amountOfWorkers how many workers shoud have the skill
     */
    public void addRequiredSkill(Skills skill, int amountOfWorkers) {
        requiredSkills.put(skill, amountOfWorkers);
    }
    
    /**
     * Adds new team for the finder.
     * @param team team to add
     */
    public void addTeam(BuildersTeam team) {
        buildersTeams.add(team);
    }
    
    /**
     * Selects teams from {@link #buildersTeams} that meet set requirements.
     */
    private void findSuitableTeams() {
        for (BuildersTeam team : buildersTeams) {
            boolean suitable = true;
            Iterator iter = requiredSkills.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry mapElement = (Map.Entry)iter.next();
                Skills skill = (Skills) mapElement.getKey();
                int workersRequired = (int) mapElement.getValue();
                if (team.searchForSkill(skill) < workersRequired) {
                    suitable = false;
                }
            }
            if (suitable) {
                suitableTeams.add(team);
            }
        }
    }
}
