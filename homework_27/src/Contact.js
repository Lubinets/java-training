import React, { Component } from 'react';
import ContactDetails from './ContactDetails';
import data from './Data';
import {Route} from 'react-router-dom';
import { Link } from 'react-router-dom';

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contactData: data
        }
    }

    render() {
        return (
            <>
                {
                    this.props.data.map((item) => {
                        return (
                            <div>
                                <Link to={"/contact/" + item.id}>{item.name}</Link>
                                <Route path={'/contact/' + item.id} render={props => (<ContactDetails data={item}/>)}/>
                            </div>
                        )
                    })
                }
            </>
        )
    }
}

export default Contact;