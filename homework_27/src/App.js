import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import { Link } from 'react-router-dom';
import data from './Data';
import Contact from './Contact';
import Home from './Home';

class App extends Component {
  render() {
    return(
      <>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/contact">Contact</Link></li>
        </ul>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/contact' render={props => (<Contact data={data}/>)}/>
        </Switch>
      </>
    )
  }
}

export default App;
