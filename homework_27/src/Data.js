export const data = [
    {
        id: 1,
        name: 'Test contact 1',
        age: 25,
        phone: 11111,
        role: 'test role 1'
    },

    {
        id: 2,
        name: 'Test contact 2',
        age: 26,
        phone: 12222,
        role: 'test role 2'
    },

    {
        id: 3,
        name: 'Test contact 3',
        age: 27,
        phone: 13333,
        role: 'test role 3'
    },

    {
        id: 4,
        name: 'Test contact 4',
        age: 28,
        phone: 14444,
        role: 'test role 4'
    },

    {
        id: 5,
        name: 'Test contact 5',
        age: 29,
        phone: 15555,
        role: 'test role 5'
    },
];

export default data;