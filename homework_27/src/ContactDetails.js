import React, { Component } from 'react';

class ContactDetails extends Component {
    render() {
        return (
            <ul>
                <li>{this.props.data.name}</li>
                <li>{this.props.data.age}</li>
                <li>{this.props.data.phone}</li>
                <li>{this.props.data.role}</li>
            </ul>
        )
    }
}

export default ContactDetails;