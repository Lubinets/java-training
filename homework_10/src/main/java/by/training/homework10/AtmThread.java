package by.training.homework10;


/**
 * Thread class for concurrent work of Atms.
 */
public class AtmThread extends Thread {
    private final Atm atm;
    private final boolean isAdding;
    private final String name;
    
    public AtmThread(String name, Atm atm, boolean isAdding) {
        if (name == null || atm == null) {
            throw new IllegalArgumentException();
        }
        this.atm = atm;
        this.isAdding = isAdding;
        this.name = name;
    }

    @Override
    public void run() {
        do {
            if (atm.showBalance() <= 0 || atm.showBalance() >= 1000) {
                return;
            }
            if (isAdding) {
                produce();
            } else {
                consume();
            }
            try {
                Thread.sleep(Main.random(4, 2) * 1000);
            } catch (InterruptedException exception) {
                Thread.currentThread().interrupt();
                exception.printStackTrace();
            }
        } while (!Thread.interrupted());
    }
    
    /**
     * Withdraws 5-10$ from card's balance.
     */
    public void consume() {
        int toDraw = Main.random(5, 6);
        atm.withdraw(toDraw);
        System.out.println(atm.showBalance() + "$----" + name
                + " withdrawn " + toDraw + "$");
    }
    
    /**
     * Adds 5-10$ to card's balance.
     */
    public void produce() {
        int toAdd = Main.random(5, 6);
        atm.add(toAdd);
        System.out.println(atm.showBalance() + "$----" + name
                + " added " + toAdd + "$");
    }
}
