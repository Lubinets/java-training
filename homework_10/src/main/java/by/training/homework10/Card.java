package by.training.homework10;

import java.util.Objects;

/**
 * Abstract representation of a bank card.
 */
public class Card {
    private volatile double balance;
    private String ownerName;

    public Card(final String ownerName) {
        if (ownerName == null) {
            throw new IllegalArgumentException();
        }
        this.ownerName = ownerName;
        this.balance = 0;
    }

    public Card(final String ownerName, final Double balance) {
        if (ownerName == null || balance < 0 || Objects.isNull(balance)) {
            throw new IllegalArgumentException();
        }
        this.ownerName = ownerName;
        this.balance = balance;
    }

    /**
     * Adds money to the balance.
     * @param currency Amount of money to add
     */
    public void add(final Double currency) {
        if (currency < 0 || Objects.isNull(currency)) {
            throw new IllegalArgumentException();
        }
        balance += currency;
    }

    /**
     * Withdraw money from balance.
     * @param currency Amount of money to withdraw
     */
    public void withdraw(final Double currency) {
        if (currency < 0 || Objects.isNull(currency)) {
            throw new IllegalArgumentException();
        }
        balance -= currency;
    }

    /**
     * Withdraws all money from balance.
     */
    public void withdrawAll() {
        balance = 0;
    }

    /**
     * Gets card balance.
     * @return returns card balance
     */
    public double showBalance() {
        return balance;
    }

    /**
     * Gets owner name.
     * @return returns card owner name
     */
    public String getOwner() {
        return ownerName;
    }

    /**
     * Shows balance of card in other currency.
     * @param exchangeRate exchange rate of currency
     * @return returns balance in other currency.
     */
    public double displayInOtherCurrency(final Double exchangeRate) {
        if (Objects.isNull(exchangeRate) || exchangeRate < 0) {
            throw new IllegalArgumentException();
        }
        return balance * exchangeRate;
    }
}
