package by.training.homework10;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Main {

    /**
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        Card card = new Card("owner", 500.0);
        int atmsNumber = random(3, 3);
        ExecutorService service = Executors.newFixedThreadPool(atmsNumber * 2);
        for (int i = 0; i < atmsNumber; i++) {
            service.submit(new AtmThread("Producer" + i, new Atm(card), true));
            service.submit(new AtmThread("Consumer" + i, new Atm(card), false));
        }
        service.shutdown();
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException exception) {
            Thread.currentThread().interrupt();
            exception.printStackTrace();
        }
        if (card.showBalance() <= 0) {
            System.out.println("No money left");
        } else if (card.showBalance() >= 1000) {
            System.out.println("Balance is more than 1000");
        }
    }
    
    /**
     * Returns pseudorandom number.
     * @param bound the upper bound
     * @param offset offset for number generation
     * @return returns pseudorandom integer number
     */
    public static int random(int bound, int offset) {
        if (bound <= 0 || offset <= 0) {
            throw new IllegalArgumentException();
        }
        Random rand = new Random();
        return rand.nextInt(bound) + offset;
    }
}
