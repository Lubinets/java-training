import {SERVER_URL} from '../constants';
import axios from 'axios';


class ActionService {
    getActionList(stateId) {
        return axios.get(SERVER_URL + "/actions?state=" + stateId)
    }

    changeState(ticketId, stateId) {
        const headers = {"Content-Type": "application/json"}
        return axios.patch(SERVER_URL + "/tickets/" + ticketId + "/?state=" + stateId, headers);
    }
}

export default new ActionService();