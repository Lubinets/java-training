import {SERVER_URL} from '../constants';
import axios from 'axios';


class AuthenticationService {
    executeAuth(username, password) {
        return axios.get(SERVER_URL + '/auth',
            { headers: {authorization: this.createAuthToken(username, password)}}
        );        
    }

    createAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ":" + password);
    }

    registerSuccessfulLogin(user, username, password) {
        localStorage.setItem('authenticatedUser', JSON.stringify(user));
        localStorage.setItem("token", this.createAuthToken(username, password));
        this.setupAxiosInterceptors();
    }

    getUser() {
        return JSON.parse(localStorage.getItem('authenticatedUser'));
    }

    isUserLoggedIn() {
        let user = localStorage.getItem('authenticatedUser');
        if (user === null) { 
            return false;
        }
        return true;
    }

    logout() {
        localStorage.removeItem('authenticatedUser');
        localStorage.removeItem('token');
    }

    setupAxiosInterceptors() {
        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers['Authorization'] = localStorage.getItem('token');
                }
                return config;
            }
        )
    }
}

export default new AuthenticationService();
