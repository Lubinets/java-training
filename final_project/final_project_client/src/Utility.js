

class Utility {
    transformKeyText(string) 
    {
        let result = string.replace(/([A-Z]+)/g, " $1").replace(/\s/, " ");
        return result.charAt(0).toUpperCase() + result.slice(1);
    }
}

export default new Utility();