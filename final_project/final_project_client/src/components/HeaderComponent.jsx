import React, { Component } from 'react';
import AuthenticationService from '../services/AuthenticationService';
import '../css/HeaderComponent.css';

class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout() {
        AuthenticationService.logout();
        window.location.reload();
    }

    render() {
        const user = AuthenticationService.getUser();
        return(
            <div id='header'>
                <h1>HelpDesk</h1>
                <div id='logout'>
                    {!AuthenticationService.isUserLoggedIn() ? '' : 
                    <>
                        <label>
                            {user.firstName + " " + user.lastName}
                        </label>
                        <button onClick={this.logout}>Logout</button>
                    </>}
                </div>
            </div>
        )
    }
}

export default HeaderComponent;