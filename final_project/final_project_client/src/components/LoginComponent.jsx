import React, { Component } from 'react';
import AuthenticationService from '../services/AuthenticationService';
import HeaderComponent from './HeaderComponent';
import '../css/LoginComponent.css';

class LoginComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            error: '',
            valid: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
        this.validate = this.validate.bind(this);
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]
                    : event.target.value
            }
        )
    }

    login(event) {
        event.preventDefault();
        if(this.validate()) {
            this.setState({ valid: true });
            AuthenticationService
            .executeAuth(this.state.username, this.state.password)
            .then((response) => {
                AuthenticationService.registerSuccessfulLogin(response.data, this.state.username, this.state.password);
                this.props.history.push('/all-tickets');
            }).catch((error) => {
                if(error.response != undefined) {
                    if(error.response.status === 401) {
                        this.setState({ error: 'Invalid username or password' });
                    }
                } else {
                    this.setState({ error: 'Cannot connect to server' });
                }
            })
        } else {
            this.setState({ valid: false });
        }
    }

    validate() {
        const emailRegexp = /^(([^\.])[\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/g;
        const passwordRegexp = /^(?=.*[~.(),:;<>@\[\]!#$%&*+-\/=?^_{|}])(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/g;
        
        if (!passwordRegexp.test(this.state.password) || !emailRegexp.test(this.state.username)) {
            return false;
        } else {
            return true;
        }
    }

    render() {
        return(
            <>
                <HeaderComponent/>
                <form onSubmit={this.login}>
                    <div className='error'>{this.state.error}</div>
                    {this.state.valid ? '' : <div className='error'>
                        Please make sure you are using a valid email password
                        </div>
                    }
                    <table>
                        <tbody>
                            <tr>
                                <td className='login-td'>
                                    <label>User Name:</label>
                                </td>
                                <td className='login-td'>
                                    <input onChange={this.handleChange} required type='text' maxLength='100' name='username'/>
                                </td>
                            </tr>
                            <tr>
                                <td className='login-td'>
                                    <label>Password:</label>
                                </td>
                                <td className='login-td'>
                                    <input onChange={this.handleChange} required type='password' minLength='6' maxLength='20' name='password'/>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td className='login-td'>
                                    <input type='submit' text='Enter'/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </>
        )
    }
}

export default LoginComponent;