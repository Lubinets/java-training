import React, { Component } from 'react';
import '../css/TicketOverview.css';
import HeaderComponent from './HeaderComponent';
import axios from 'axios';
import {SERVER_URL} from '../constants';
import AuthenticationService from '../services/AuthenticationService';
import Hisotry from './History';
import Comments from './Comments';
import Utility from '../Utility';
import EditTicket from './EditTicket';

class TicketOverview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ticket: '',
            active: 'history',
            editForm: false
        };

        this.onClickComments = this.onClickComments.bind(this);
        this.onClickHistory = this.onClickHistory.bind(this);
        this.onClickTicketList = this.onClickTicketList.bind(this);
        this.onClickEdit = this.onClickEdit.bind(this);
        this.deleteAttachment = this.deleteAttachment.bind(this);
        this.openFeedback = this.openFeedback.bind(this);

        this.history = React.createRef();
        this.comments = React.createRef();
    }

    componentDidMount() {
        AuthenticationService.setupAxiosInterceptors();
        axios.get(SERVER_URL + "/tickets/" + this.props.match.params.id)
        .then(response => {
            this.setState({ticket: response.data});
        });
    }

    onClickHistory() {
        this.history.current.classList.add("active");
        this.comments.current.classList.remove("active");
        this.setState({
            active: 'history'
        });
    }

    onClickComments() {
        this.comments.current.classList.add("active");
        this.history.current.classList.remove("active");
        this.setState({
            active: 'comments'
        });
    }

    onClickTicketList() {
        this.props.history.push('/all-tickets');
    }

    onClickEdit() {
        this.setState({
            editForm: true
        })
    }

    closeEditWindow = () => {
        this.setState({
            editForm: false
        })
    }

    deleteAttachment(e) {
        axios.delete(SERVER_URL + "/attachments/" + e.target.getAttribute('id'))
        .then(reponse => {
            window.location.reload();
        }).catch(error => {
            alert("Cannot delete this attachments");
        })
    }

    openFeedback() {
        this.props.history.push({
            pathname :'/feedback/' + this.props.match.params.id,
            state : {edit: true}
        });
    }

    render() {
        let keys = Object.keys(this.state.ticket);
        keys = keys.filter(function(value) {
            return value != "attachments"
        });
        let attachments = null;
        if(this.state.ticket.attachments != undefined) {
            attachments = this.state.ticket.attachments.map(item => 
                <li key={item}>
                    <a href={"/download/" + item}target="_blank">Attachment {item}</a>
                    <button id={item} className="delete-btn" onClick={this.deleteAttachment}>X</button>
                </li>
            )
        } else {
            attachments = '';
        }

        return(
            <>
                <HeaderComponent/>
                {
                    this.state.editForm
                    ? <EditTicket ticketId={this.state.ticket.id} closeFunction={this.closeEditWindow}/> : ''
                }
                <div className="right">
                    {
                        this.state.ticket.state === "DRAFT"
                        ? <button onClick={this.onClickEdit}>Edit</button> : ''
                    }
                    {
                        this.state.ticket.state === "DONE"
                        ? <button onClick={this.openFeedback}>Leave Feedback</button> : ''
                    }
                </div>
                <div id="overview-div" className="left">
                    <button onClick={this.onClickTicketList}>Ticket List</button>
                    <h2>{this.state.ticket.name}</h2>
                    <ul>
                        {
                            keys.map(item =>           
                              <li key={item}><span className='bold'>{Utility.transformKeyText(item)}</span>{": " + this.state.ticket[item]}</li>
                            )          
                        }
                    </ul>
                    <h3>Attachments:</h3>
                    <ul id="attachments">
                        {attachments}
                    </ul>
                </div>
                <div className="btn-block">
                    <button className="active" onClick={this.onClickHistory} ref={this.history}>History</button>
                    <button onClick={this.onClickComments} ref={this.comments}>Commnets</button>
                </div>
                {this.state.active === 'history' 
                    ? <Hisotry id={this.props.match.params.id}/>
                    : <Comments id={this.props.match.params.id}/>
                }
            </>
        )
    }
}

export default TicketOverview;