import React, { Component } from 'react';
import '../css/CreateTicket.css';
import axios from 'axios';
import { SERVER_URL} from '../constants';
import CreateTicket from './CreateTicket';

class EditTicket extends CreateTicket {
    constructor(props) {
        super(props);
        this.state.header = "Edit Ticket";
    }

    onSubmitDraft(e) {
        e.preventDefault();
        const formData = new FormData();
        const ticket = new Blob([JSON.stringify(this.state.formFields)], {type: "application/json"});
        formData.append('ticket', ticket);
        formData.append("file", this.state.file);
        const headers = {'Content-Type': undefined};
        axios.post(SERVER_URL + '/tickets/' + this.props.ticketId + "?draft=true", formData, headers)
        .then(response => {
            alert(response.statusText);
            this.props.closeFunction();
        }).catch(error => {
            alert(error.response.data)
        });
    }

    onSubmitNew(e) {
        e.preventDefault();
        const formData = new FormData();
        const ticket = new Blob([JSON.stringify(this.state.formFields)], {type: "application/json"});
        formData.append('ticket', ticket);
        formData.append("file", this.state.file);
        const headers = {'Content-Type': undefined};
        
        axios.post(SERVER_URL + '/tickets/' + this.props.ticketId + "?draft=false", formData, headers)
        .then(response => {
            alert(response.statusText);
            this.props.closeFunction();
        }).catch(error => {
            alert(error.response.data)
        });
    }
}

export default EditTicket;