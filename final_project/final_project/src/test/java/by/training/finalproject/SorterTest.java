package by.training.finalproject;

import by.training.finalproject.utility.Sorter;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SorterTest {
    @Mock
    CriteriaBuilder mockBuilder;
    
    @Mock
    Root mockRoot;
    
    @Mock
    Path mockPath;
    
    @Mock
    Order mockOrder;
    
    @Before
    public void setup() {
        Mockito.when(mockBuilder.desc(mockPath)).thenReturn(mockOrder);
        Mockito.when(mockRoot.get("name")).thenReturn(mockPath);
    }
    
    @Test
    public void buildTest() {
        Sorter sorter = new Sorter("name,desc");
        Order actual = sorter.build(mockBuilder, mockRoot);
        
        Order expected = mockBuilder.desc(mockRoot.get("name"));
        Assert.assertEquals(expected, actual);
    }  
}
