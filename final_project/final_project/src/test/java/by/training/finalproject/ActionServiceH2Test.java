package by.training.finalproject;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Action;
import by.training.finalproject.model.Role;
import by.training.finalproject.model.State;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.ActionDao;
import by.training.finalproject.repository.UserDao;
import by.training.finalproject.service.ActionService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ActionServiceH2Test {
    @Mock
    private ActionDao mockActionDao;
    
    @Mock
    private UserDao mockUserDao;
    
    @InjectMocks
    private ActionService service;
    
    @Test
    public void  getAllAllowed() throws NotFoundException {
        List<Action> actionsFromMock = new ArrayList();
        actionsFromMock.add(new Action());
        User user = new User();
        user.setRole(Role.ROLE_EMPLOYEE);
        Mockito.when(mockActionDao.getAllAllowed(State.NEW, Role.ROLE_EMPLOYEE)).thenReturn(actionsFromMock);
        Mockito.when(mockUserDao.getByName("name")).thenReturn(Optional.ofNullable(user));
        
        List<Action> actions = service.getAllAllowed(State.NEW, "name");
        Assert.assertEquals(actionsFromMock, actions);
    }
    
    @Test(expected = NotFoundException.class)
    public void  getAllAllowedNegative() throws NotFoundException {
        List<Action> actionsFromMock = new ArrayList();
        User user = new User();
        user.setRole(Role.ROLE_EMPLOYEE);
        Mockito.when(mockActionDao.getAllAllowed(State.APPROVED, Role.ROLE_EMPLOYEE)).thenReturn(actionsFromMock);
        Mockito.when(mockUserDao.getByName("name")).thenReturn(Optional.ofNullable(user));
        
        List<Action> actions = service.getAllAllowed(State.APPROVED, "name");
        Assert.assertEquals(actionsFromMock, actions);
    }
}
