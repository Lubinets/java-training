package by.training.finalproject.service;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Role;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.UserDao;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class UserService {
    @Autowired
    private UserDao userDao;
    
    public User getUserByName(String username) throws NotFoundException {
        Optional<User> userOpt = userDao.getByName(username);
        if(userOpt.isPresent()) {
            return userOpt.get();
        } else {
            throw new NotFoundException();
        }
    }
    
    public List<User> getByRole(Role role) throws NotFoundException {
        List<User> users = userDao.getByRole(role);
        if(!users.isEmpty()) {
            return users;
        } else {
            throw new NotFoundException();
        }
    }
}
