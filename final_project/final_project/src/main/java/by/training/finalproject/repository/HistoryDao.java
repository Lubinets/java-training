package by.training.finalproject.repository;

import by.training.finalproject.model.History;
import java.util.List;


public interface HistoryDao {
    /**
     * Saves new history record into database.
     * @param historyRecord new history record.
     */
    public void save(History historyRecord);
    
    /**
     * Gets history of changes made to ticket.
     * @param ticketId ticket id
     * @return list of history records
     */
    public List<History> getHistoryList(long ticketId);
}
