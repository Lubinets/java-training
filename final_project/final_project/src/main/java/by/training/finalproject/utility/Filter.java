package by.training.finalproject.utility;

import by.training.finalproject.model.State;
import by.training.finalproject.model.Urgency;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


public class Filter {
    private final Map<String, String[]> paramMap;
    
    private final Map<String, Class> constraints;
    
    public Filter(Map<String, String[]> paramMap) {
        this.paramMap = paramMap;
        constraints = new HashMap();
    }
    
    /**
     * Sets allowed params from http request.
     * @param paramName name of the parameter
     * @param paramClass class of the parameter in mapped class
     */
    public void setConstraint(String paramName, Class paramClass) {
        constraints.put(paramName, paramClass);
    }
    
    /**
     * Forms list of predicates for hibernate query from http request params.
     * @param builder criteria builder
     * @param root query root
     * @return list of predicates
     */
    public List<Predicate> build(CriteriaBuilder builder, Root root) {
        List<Predicate> predicates = new ArrayList<>();
        if(constraints.isEmpty()) {
            paramMap.entrySet().forEach((entry) -> {
                for (String value : entry.getValue()) {
                    predicates.add(builder.equal(root.get(entry.getKey()), value));
                }
            });
        } else {
            paramMap.entrySet().forEach((entry) -> {
                Class paramClass = constraints.get(entry.getKey());
                if (!Objects.isNull(paramClass)) {
                    List<Predicate> multipleParams = new ArrayList<>();
                    for (String value : entry.getValue()) {
                        multipleParams.add(builder.equal(root.get(entry.getKey()),
                                parse(value, paramClass)));
                    }
                    predicates.add(builder.or(multipleParams.toArray(new Predicate[predicates.size()])));
                }
            });
        }
        return predicates;
    }
    
    private Object parse(String value, Class className) {
        if (className == Integer.class) {
            return Integer.valueOf(value);
        } else if (className == Date.class) {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            return format.parse(value, new ParsePosition(0));
        } else if (className == State.class) {
            return State.valueOf(value);
        } else if (className == Urgency.class) {
            return Urgency.valueOf(value);
        } else {
            return value;
        }
    }
}
