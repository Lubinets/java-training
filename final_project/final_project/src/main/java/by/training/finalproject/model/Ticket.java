package by.training.finalproject.model;

import by.training.finalproject.model.dto.TicketCreateDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "Ticket")
public class Ticket implements Serializable {
    private final static String regexp = "^[a-zA-Z0-9~.(),:;<>@\\[\\]!#$%&*+-\\/=?^_{|}]+$";
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "name")
    @Basic(optional = false)
    @Size(max = 100, message = "Ticket name shouldn't be more than 100 characters")
    @NotNull(message = "Ticket name can't be empty")
    @Pattern(regexp = regexp, message = "Invalid ticket name format")
    private String name;
    
    @Column(name = "description", nullable = true)
    @Size(max = 500, message = "Description shouldn't be more than 500 characters")
    @Pattern(regexp = regexp, message = "Invalid description format")
    private String description;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "created_on")
    private Date createdOn;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "desired_resolution_date", nullable = true)
    private Date desiredResolutionTime;
    
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User ticketOwner;
    
    @ManyToOne
    @JoinColumn(name = "assignee_id", nullable = true)
    private User ticketAssignee;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "state_id")
    private State state;
    
    @ManyToOne
    @NotNull(message = "Category can't be empty")
    @JoinColumn(name = "category_id")
    private Category category;
    
    @Enumerated(EnumType.ORDINAL)
    @NotNull(message = "Urgency can't be empty")
    @Column(name = "urgency_id")
    private Urgency urgency;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "approver_id", nullable = true)
    private User approver;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
    @Cascade({org.hibernate.annotations.CascadeType.MERGE,
        org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Comment> comments = new ArrayList();
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ticket")
    @Cascade({org.hibernate.annotations.CascadeType.MERGE,
        org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Attachment> attachments = new ArrayList();
    
    @OneToOne(cascade = CascadeType.MERGE, mappedBy = "ticket")
    private Feedback feedback;
    
    
    
    
    public Ticket() { }
    
    public Ticket(TicketCreateDTO dto, User owner) {
        if(Objects.isNull(dto)) {
            throw new IllegalArgumentException();
        }
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.createdOn = new Date();
        this.desiredResolutionTime = dto.getDesiredResolutionDate();
        this.category = dto.getCategory();
        this.urgency = dto.getUrgency();
        this.ticketOwner = owner;
        if(!Objects.isNull(dto.getComment())) {
            Comment comment = new Comment();
            comment.setText(dto.getComment());
            comment.setDate(createdOn);
            comment.setUser(owner);
            comment.setTicket(this);
            comments.add(comment);
        }
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the desiredResolutionTime
     */
    public Date getDesiredResolutionTime() {
        return desiredResolutionTime;
    }

    /**
     * @param desiredResolutionTime the desiredResolutionTime to set
     */
    public void setDesiredResolutionTime(Date desiredResolutionTime) {
        this.desiredResolutionTime = desiredResolutionTime;
    }

    /**
     * @return the ticketOwner
     */
    public User getTicketOwner() {
        return ticketOwner;
    }

    /**
     * @param ticketOwner the ticketOwner to set
     */
    public void setTicketOwner(User ticketOwner) {
        this.ticketOwner = ticketOwner;
    }

    /**
     * @return the ticketAssignee
     */
    public User getTicketAssignee() {
        return ticketAssignee;
    }

    /**
     * @param ticketAssignee the ticketAssignee to set
     */
    public void setTicketAssignee(User ticketAssignee) {
        this.ticketAssignee = ticketAssignee;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return the urgency
     */
    public Urgency getUrgency() {
        return urgency;
    }

    /**
     * @param urgency the urgency to set
     */
    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    /**
     * @return the approver
     */
    public User getApprover() {
        return approver;
    }

    /**
     * @param approver the approver to set
     */
    public void setApprover(User approver) {
        this.approver = approver;
    }

    /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * @return the attachments
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * @param attachments the attachments to set
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }
    
    public void addAttachment(Attachment attachment) {
        attachment.setTicket(this);
        this.attachments.add(attachment);
    }

    /**
     * @return the feedback
     */
    public Feedback getFeedback() {
        return feedback;
    }

    /**
     * @param feedback the feedback to set
     */
    public void setFeedback(Feedback feedback) {
        feedback.setTicket(this);
        this.feedback = feedback;
    }
}
