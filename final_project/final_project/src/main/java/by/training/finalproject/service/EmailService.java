package by.training.finalproject.service;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Action;
import by.training.finalproject.model.Role;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.User;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Service
public class EmailService {

    private final Logger logger = LogManager.getLogger(this.getClass());

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private UserService userService;
    
    @Async
    public void sendEmail(Action action, Ticket ticket) {
        if(Objects.nonNull(action.getMailTemplate())) {
            try {
                switch (action.getMailTemplate()) {
                    case ("NewTicketProvidedTemplate"):
                        sendNewTicket(ticket);
                        break;
                    case ("TicketApprovedTemplate"):
                        sendTicketApproved(ticket);
                        break;
                    case ("TicketCancelledEngineerTemplate"):
                        sendTicketCancelledEngineer(ticket);
                        break;
                    case ("TicketCancelledManagerTemplate"):
                        sendTicketCancelledManager(ticket);
                        break;
                    case ("TicketDeclinedTemplate"):
                        sendTicketDeclined(ticket);
                        break;
                    case ("TicketDoneTemplate"):
                        sendTicketDone(ticket);
                        break;
                    default:
                        return;
                }
            } catch (Exception ex) {
                logger.warn(ex);
            }
        }
    }

    @Async
    public void sendFeedbackProvided(Ticket ticket) {
        try {
            String template = "FeedbackProvidedTemplate";
            Context context = new Context();
            context.setVariable("id", ticket.getId());
            context.setVariable("user", ticket.getTicketAssignee());
            String subject = "Feedback was provided";
            Address address = new InternetAddress(ticket.getTicketAssignee().getEmail());

            MimeMessage message = setupMimeMessage(template, context,
                    subject, address);
            emailSender.send(message);
        } catch (Exception ex) {
            logger.warn(ex);
        }
    }

    @Async
    public void sendNewTicket(Ticket ticket) {
        try {
            String template = "NewTicketTemplate";
            Context context = new Context();
            context.setVariable("id", ticket.getId());
            String subject = "New ticket for approval";
            List<User> recipients = userService.getByRole(Role.ROLE_MANAGER);
            Address[] addresses = getMailList(recipients);

            MimeMessage message = setupMimeMessage(template, context,
                    subject, addresses);
            emailSender.send(message);
        } catch (Exception ex) {
            logger.warn(ex);
        }
    }

    private void sendTicketApproved(Ticket ticket) throws MessagingException, NotFoundException {
        String template = "TicketApprovedTemplate";
        Context context = new Context();
        context.setVariable("id", ticket.getId());
        String subject = "Ticket was approved";
        List<User> recipients = userService.getByRole(Role.ROLE_ENGINEER);
        recipients.add(ticket.getTicketOwner());
        Address[] addresses = getMailList(recipients);
        
        MimeMessage message = setupMimeMessage(template, context,
                subject, addresses);
        emailSender.send(message);
    }

    private void sendTicketCancelledEngineer(Ticket ticket) throws MessagingException {
        String template = "TicketCancelledEngineerTemplate";
        Context context = new Context();
        context.setVariable("id", ticket.getId());
        context.setVariable("user", ticket.getTicketOwner());
        String subject = "Ticket was cancelled";
        Address address = new InternetAddress(ticket.getTicketOwner().getEmail());
        
        MimeMessage message = setupMimeMessage(template, context,
                subject, address);
        emailSender.send(message);
    }

    private void sendTicketCancelledManager(Ticket ticket) throws MessagingException {
        String template = "TicketCancelledManagerTemplate";
        Context context = new Context();
        context.setVariable("id", ticket.getId());
        context.setVariable("user", ticket.getTicketOwner());
        String subject = "Ticket was cancelled";
        Address address = new InternetAddress(ticket.getTicketOwner().getEmail());
        
        MimeMessage message = setupMimeMessage(template, context,
                subject, address);
        emailSender.send(message);
    }

    private void sendTicketDeclined(Ticket ticket) throws MessagingException {
        String template = "TicketDeclinedTemplate";
        Context context = new Context();
        context.setVariable("id", ticket.getId());
        context.setVariable("user", ticket.getTicketOwner());
        String subject = "Ticket was declined";
        Address address = new InternetAddress(ticket.getTicketOwner().getEmail());
        
        MimeMessage message = setupMimeMessage(template, context,
                subject, address);
        emailSender.send(message);
    }

    private void sendTicketDone(Ticket ticket) throws MessagingException {
        String template = "TicketDoneTemplate";
        Context context = new Context();
        context.setVariable("id", ticket.getId());
        context.setVariable("user", ticket.getTicketOwner());
        String subject = "Ticket was done";
        Address address = new InternetAddress(ticket.getTicketOwner().getEmail());
        
        MimeMessage message = setupMimeMessage(template, context,
                subject, address);
        emailSender.send(message);
    }

    private Address[] getMailList(List<User> users) throws AddressException {
        List<Address> addresses = new ArrayList();
        for (User user : users) {
            addresses.add(new InternetAddress(user.getEmail()));
        }
        return addresses.toArray(new Address[addresses.size()]);
    }
    
    private MimeMessage setupMimeMessage(String templateName, Context context, 
            String subject, Address... addresses) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        
        String htmlContent = this.templateEngine.process(templateName, context);
        
        message.setRecipients(Message.RecipientType.TO, addresses);
        helper.setText(htmlContent, true);
        helper.setSubject(subject);
        helper.setFrom("homework28lubinets@gmail.com");
        
        return message;
    }
}
