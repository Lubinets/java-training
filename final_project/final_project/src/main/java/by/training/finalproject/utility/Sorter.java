package by.training.finalproject.utility;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;


public class Sorter {
    private String name;
    
    private SortingType type;
    
    public Sorter(String query) throws IllegalArgumentException {
        try {
            String[] tokens = query.split(",");
            this.name = tokens[0];
            this.type = SortingType.fromString(tokens[1]);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
    
    /**
     * Builds Order object from http request sort param.
     * @param builder criteria builder
     * @param root query root
     * @return order object that defines the ordering of query results.
     */
    public Order build(CriteriaBuilder builder, Root root) {
        if(type.equals(SortingType.ASC)) {
            return builder.asc(root.get(name));
        } else if (type.equals(SortingType.DESC)) {
            return builder.desc(root.get(name));
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    private enum SortingType {
        ASC("asc"),
        DESC("desc");
        
        private String text;
        
        SortingType(String text) {
            this.text = text;
        }
        
        public static SortingType fromString(String text) {
            for(SortingType type : SortingType.values()) {
                if (type.text.equalsIgnoreCase(text)) {
                    return type;
                }
            }
            return null;
        }
    }
}
