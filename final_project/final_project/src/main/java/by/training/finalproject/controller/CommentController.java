package by.training.finalproject.controller;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Comment;
import by.training.finalproject.model.dto.CommentDTO;
import by.training.finalproject.service.CommentService;
import java.lang.reflect.Type;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/comments", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/tickets/{id}")
    public ResponseEntity<List<CommentDTO>> getByTicketId(
            @PathVariable(value = "id") long id) throws NotFoundException {
        Type listType = new TypeToken<List<CommentDTO>>() {
        }.getType();
        return ResponseEntity.ok(modelMapper.map(commentService.getByTicketId(id), listType));
    }
}
