package by.training.finalproject.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Action")
public class Action implements Serializable {
    @Id
    @Column(name = "id")
    private long id;
    
    @Column(name = "name")
    private String name;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "role_id")
    private Role role;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "old_state")
    private State oldState;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "new_state")
    private State newState;
    
    @Column(name = "mail_template")
    private String mailTemplate;
    
    public Action() { }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the oldState
     */
    public State getOldState() {
        return oldState;
    }

    /**
     * @param oldState the oldState to set
     */
    public void setOldState(State oldState) {
        this.oldState = oldState;
    }

    /**
     * @return the newState
     */
    public State getNewState() {
        return newState;
    }

    /**
     * @param newState the newState to set
     */
    public void setNewState(State newState) {
        this.newState = newState;
    }

    /**
     * @return the mailTemplate
     */
    public String getMailTemplate() {
        return mailTemplate;
    }

    /**
     * @param mailTemplate the mailTemplate to set
     */
    public void setMailTemplate(String mailTemplate) {
        this.mailTemplate = mailTemplate;
    }

}
