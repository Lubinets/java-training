package by.training.finalproject.exception;

public class NoPermissionException extends Exception {

    /**
     * Creates a new instance of <code>NoPermissionException</code> without
     * detail message.
     */
    public NoPermissionException() {
    }

    /**
     * Constructs an instance of <code>NoPermissionException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoPermissionException(String msg) {
        super(msg);
    }
}
