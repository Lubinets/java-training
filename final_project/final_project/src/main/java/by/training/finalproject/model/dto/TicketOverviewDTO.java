package by.training.finalproject.model.dto;

import by.training.finalproject.model.Attachment;
import by.training.finalproject.model.Category;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Urgency;
import by.training.finalproject.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TicketOverviewDTO {
    private long id;
    
    private String name;
    
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date createdOn;
    
    private State state;
    
    private String category;
    
    private Urgency urgency;
    
    private String description;
        
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date desiredResolutionDate;
    
    private String ticketOwner;
    
    private String approver;
    
    private String ticketAssignee;
    
    private List<Long> attachments;

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the urgency
     */
    public Urgency getUrgency() {
        return urgency;
    }

    /**
     * @param urgency the urgency to set
     */
    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    /**
     * @return the desiredResolutionDate
     */
    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    /**
     * @param desiredResolutionDate the desiredResolutionDate to set
     */
    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    /**
     * @return the ticketOwner
     */
    public String getTicketOwner() {
        return ticketOwner;
    }

    /**
     * @param ticketOwner the ticketOwner to set
     */
    public void setTicketOwner(User ticketOwner) {
        this.ticketOwner = ticketOwner.getFirstName() + " " + ticketOwner.getLastName();
    }

    /**
     * @return the approver
     */
    public String getApprover() {
        return approver;
    }

    /**
     * @param approver the approver to set
     */
    public void setApprover(User approver) {
        this.approver = approver.getFirstName() + " " + approver.getLastName();
    }

    /**
     * @return the ticketAssignee
     */
    public String getTicketAssignee() {
        return ticketAssignee;
    }

    /**
     * @param ticketAssignee the ticketAssignee to set
     */
    public void setTicketAssignee(User ticketAssignee) {
        this.ticketAssignee = ticketAssignee.getFirstName() + " " + ticketAssignee.getLastName();
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the Category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param Category the Category to set
     */
    public void setCategory(Category category) {
        this.category = category.getName();
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the attachments
     */
    public List<Long> getAttachments() {
        return attachments;
    }

    /**
     * @param attachments the attachments to set
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = new ArrayList();
        for(Attachment attachment : attachments) {
            this.attachments.add(attachment.getId());
        }
    }
}
