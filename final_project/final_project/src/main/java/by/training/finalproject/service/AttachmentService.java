package by.training.finalproject.service;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Attachment;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.AttachmentDao;
import java.io.IOException;
import java.security.Principal;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class AttachmentService {
    @Autowired
    private AttachmentDao dao;
    
    @Autowired
    private HistoryService historyService;
    
    @Autowired
    private UserService userService;
    
    public void validateFile(MultipartFile file) {
        Long maxSize = 5000000L;
        if(file.getSize() > maxSize) {
            throw new TransactionSystemException("File size should be less than 5MB");
        }
    }
    
    public Attachment multipartToAttachment(MultipartFile file) throws IOException {
        Attachment attachment = new Attachment();
        attachment.setName(file.getOriginalFilename());
        attachment.setBlob(file.getBytes());
        attachment.setContentType(file.getContentType());
        return attachment;
    }
    
    @Transactional
    public Attachment getById(long id) throws NotFoundException {
        Optional<Attachment> attachmentOpt = dao.getById(id);
        if(attachmentOpt.isPresent()) {
            return attachmentOpt.get();
        } else {
            throw new NotFoundException();
        }
    }
    
    @Transactional
    public void deleteAttachment(long id, Principal principal)
            throws NotFoundException, NoPermissionException {
        User user = userService.getUserByName(principal.getName());
        Optional<Attachment> attachmentOpt = dao.getById(id);       
        if(!attachmentOpt.isPresent()) {
            throw new NotFoundException();
        }
        
        Attachment attachment = attachmentOpt.get();
        Ticket ticket = attachment.getTicket();
        if(!ticket.getTicketOwner().equals(user)) {
            throw new NoPermissionException();
        }
        
        dao.removeAttachment(id);
        historyService.addAttachmentDeletedRecord(ticket, user, attachment);
        
    }
}
