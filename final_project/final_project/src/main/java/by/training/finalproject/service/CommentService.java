package by.training.finalproject.service;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Comment;
import by.training.finalproject.repository.CommentDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommentService {
    @Autowired
    private CommentDao commentDao;
    
    @Transactional
    public List<Comment> getByTicketId(long id) throws NotFoundException {
        List<Comment> comments = commentDao.getByTicketId(id);
        if(!comments.isEmpty()) {
            return comments;
        } else {
            throw new NotFoundException();
        }
    }
}
