package by.training.finalproject.model;

public enum State {
    DRAFT(0),
    NEW(1),
    APPROVED(2),
    DECLINED(3),
    CANCELLED(4),
    INPROGRESS(5),
    DONE(6);
    
    private final int id;

    State(int id) {
        this.id = id;
    }
    

    public int getId() {
        return id;
    }
    
    public static State valueOf(int id) {
        State[] states = State.values();
        for(int i = 0; i < states.length; i++) {
            if(states[i].getId() == id) {
                return states[i];
            }
        }
        return null;
    }
}
