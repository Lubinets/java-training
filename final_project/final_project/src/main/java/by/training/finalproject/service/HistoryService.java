package by.training.finalproject.service;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Attachment;
import by.training.finalproject.model.History;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.HistoryDao;
import java.util.Date;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class HistoryService {
    private final Logger logger = LogManager.getLogger(this.getClass());
    
    @Autowired
    private HistoryDao dao;
    
    @Transactional
    public List<History> getHistoryList(long id) throws NotFoundException {
        List<History> records = dao.getHistoryList(id);
        if(!records.isEmpty()) {
            return records;
        } else {
            throw new NotFoundException();
        }
    }
    
    public void addEditRecord(Ticket ticket) {
        History history = new History();
        history.setTicket(ticket);
        history.setAction("Ticket is edited");
        history.setDescription("Ticket is edited");
        history.setDate(new Date());
        history.setUser(ticket.getTicketOwner());
        dao.save(history);
        logger.info("Ticket " + ticket.getId() + " was edited");
    }
    
    public void addCreateRecord(Ticket ticket) {
        History history = new History();
        history.setTicket(ticket);
        history.setAction("Ticket is created");
        history.setDescription("Ticket is created");
        history.setDate(new Date());
        history.setUser(ticket.getTicketOwner());
        dao.save(history);
        logger.info("Ticket " + ticket.getId() + " was created");
    }
    
    public void addStatusChangeRecord(Ticket ticket, State oldState, User user) {
        History history = new History();
        history.setTicket(ticket);
        history.setAction("Ticket Status is changed");
        history.setDescription("Ticket Status changed from " + oldState + " to "
                + ticket.getState());
        history.setDate(new Date());
        history.setUser(user);
        dao.save(history);
        logger.info("Ticket " + ticket.getId() + " was changed from " 
                + oldState + " to " + ticket.getState());
    }
    
    public void addAttachmentAddedRecord(Ticket ticket, User user, Attachment attachment) {
        History history = new History();
        history.setTicket(ticket);
        history.setAction("File is attached");
        history.setDescription("File is attached: " + attachment.getName());
        history.setDate(new Date());
        history.setUser(user);
        dao.save(history);
        logger.info("New attachment added to ticket " + ticket.getId());
    }
    
    public void addAttachmentDeletedRecord(Ticket ticket, User user, Attachment attachment) {
        History history = new History();
        history.setTicket(ticket);
        history.setAction("File is deleted");
        history.setDescription("File is deleted: " + attachment.getName());
        history.setDate(new Date());
        history.setUser(user);
        dao.save(history);
        logger.info("Attachment was deleted from ticket " + ticket.getId());
    }
}
