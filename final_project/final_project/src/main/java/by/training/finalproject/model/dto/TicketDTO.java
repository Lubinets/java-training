package by.training.finalproject.model.dto;

import by.training.finalproject.model.State;
import by.training.finalproject.model.Urgency;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;


public class TicketDTO {
    private long id;
    
    private String name;
    
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date desiredResolutionTime;
    
    private Urgency urgency;
    
    private State state;
    

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the desiredResolutionTime
     */
    public Date getDesiredResolutionTime() {
        return desiredResolutionTime;
    }

    /**
     * @param desiredResolutionTime the desiredResolutionTime to set
     */
    public void setDesiredResolutionTime(Date desiredResolutionTime) {
        this.desiredResolutionTime = desiredResolutionTime;
    }

    /**
     * @return the urgency
     */
    public Urgency getUrgency() {
        return urgency;
    }

    /**
     * @param urgency the urgency to set
     */
    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }
}
