package by.training.finalproject.repository;

import by.training.finalproject.model.Ticket;
import by.training.finalproject.utility.Filter;
import by.training.finalproject.utility.Sorter;
import java.util.List;
import java.util.Optional;


public interface TicketDao {
    /**
     * Saves ticket into database.
     * @param ticket ticket to save
     * @return id of the ticket
     */
    public long save(Ticket ticket);
    
    /**
     * Gets ticket by id.
     * @param id id of the ticket
     * @return Optional of ticket
     */
    public Optional<Ticket> get(long id);
    
    /**
     * Gets list of all tickets.
     * @return list of all tickets
     */
    public List<Ticket> getAll();
    
    /**
     * Updates ticket.
     * @param ticket updated version of ticket
     */
    public void update(Ticket ticket);
    
    /**
     * Gets list of tickets with filtration and sorting.
     * @param filter filter to apply
     * @param sorter sorter to apply
     * @return List of all tickets filtrated and sorted
     */
    public List<Ticket> getAll(Filter filter, Sorter sorter);
    
    /**
     * Gets tickets by creator.
     * @param id id of the creator
     * @return list of tickets by creator
     */
    public List<Ticket> getByOwner(long id);
    
    /**
     * Gets tickets by creator with filtration and sorting.
     * @param id id of the creator
     * @param filter filter to apply
     * @param sorter sorter to apply
     * @return List of tickets by creator filtrated and sorted
     */
    public List<Ticket> getByOwner(long id, Filter filter, Sorter sorter);
    
    /**
     * Gets all tickets relevant for engineer.
     * @param id id of the enginner
     * @return list of tickets
     */
    public List<Ticket> getEngineerTickets(long id);
    
    /**
     * Gets tickets relevant for engineer with filtration and sorting.
     * @param id id of the engineer
     * @param filter filter to apply
     * @param sorter sorter to apply
     * @return list of tickets filtered and sorted
     */
    public List<Ticket> getEngineerTickets(long id, Filter filter, Sorter sorter);
    
    /**
     * Gets all tickets relevant to manager.
     * @param id id of the manager
     * @return list of tickets
     */
    public List<Ticket> getManagerRelevantTickets(long id);
    
    /**
     * 
     * @param id
     * @param filter
     * @param sorter
     * @return 
     */
    public List<Ticket> getManagerRelevantTickets(long id, Filter filter, Sorter sorter);
    
    /**
     * Gets all tickets assigned to the user.
     * @param id id of assignee
     * @return list of tickets
     */
    public List<Ticket> getByAssignee(long id);
    
    /**
     * Gets tickets assigned to the user with filtration and sorting.
     * @param id id of assignee
     * @param filter filter to apply
     * @param sorter sorter to apply
     * @return list of tickets filtered and sorted
     */
    public List<Ticket> getByAssignee(long id, Filter filter, Sorter sorter);
    
    /**
     * Gets tickets created or approved by manager.
     * @param id id of the manager
     * @return list of tickets
     */
    public List<Ticket> getManagerTickets(long id);
    
    /**
     * Gets tickets created or approved by manager with filtration and sorting.
     * @param id id of the manager
     * @param filter filter to apply
     * @param sorter sorter to apply
     * @return list of tickets filtered and sorted
     */
    public List<Ticket> getManagerTickets(long id, Filter filter, Sorter sorter);
}
