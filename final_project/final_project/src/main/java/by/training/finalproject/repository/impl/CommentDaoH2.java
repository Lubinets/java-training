package by.training.finalproject.repository.impl;

import by.training.finalproject.model.Comment;
import by.training.finalproject.repository.*;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDaoH2 implements CommentDao{
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public List<Comment> getByTicketId(long id) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Comment where ticket.id =:id "
                        + "order by date desc", Comment.class);
        query.setMaxResults(5);
        query.setParameter("id", id);
        return query.list();
    }
}
