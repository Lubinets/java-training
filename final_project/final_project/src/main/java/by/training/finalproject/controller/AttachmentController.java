package by.training.finalproject.controller;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Attachment;
import by.training.finalproject.service.AttachmentService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/attachments")
@CrossOrigin(exposedHeaders = {"Content-Disposition"})
public class AttachmentController {
    @Autowired
    private AttachmentService service;
    
    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getByTicketId(@PathVariable(value = "id") long id) 
            throws NotFoundException {
        Attachment attachment = service.getById(id);
        HttpHeaders header = new HttpHeaders();
        
        header.setContentType(MediaType.valueOf(attachment.getContentType()));
        header.setContentLength(attachment.getBlob().length);
        header.set("Content-Disposition", "attachment; filename=" + attachment.getName());
        return new ResponseEntity<>(attachment.getBlob(), header, HttpStatus.OK);
    }
    
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> deleteAttachment(
            @PathVariable(value = "id") long id,
            Principal principal)
            throws NotFoundException, NoPermissionException {
        service.deleteAttachment(id, principal);
        return ResponseEntity.noContent().build();
    }
}
