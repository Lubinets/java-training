package by.training.finalproject.repository;

import by.training.finalproject.model.Comment;
import java.util.List;

public interface CommentDao {
    /**
     * Returns all comments associated with ticket.
     * @param id id of the ticket
     * @return List of comments
     */
    public List<Comment> getByTicketId(long id);
}
