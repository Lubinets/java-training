package by.training.finalproject.repository;

import by.training.finalproject.model.Category;
import java.util.List;


public interface CategoryDao {
    /**
     * Returns all categories.
     * @return List of categories
     */
    public List<Category> getAll();
}
