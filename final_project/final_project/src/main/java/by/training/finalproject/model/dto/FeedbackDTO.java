package by.training.finalproject.model.dto;


public class FeedbackDTO {
    private short rate;
    
    private String text;
    
    public FeedbackDTO() { }
    
    public FeedbackDTO(short rate, String text) {
        this.rate = rate;
        this.text = text;
    }

    /**
     * @return the rate
     */
    public short getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(short rate) {
        this.rate = rate;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
}
