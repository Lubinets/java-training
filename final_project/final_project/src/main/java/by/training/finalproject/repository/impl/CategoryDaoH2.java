package by.training.finalproject.repository.impl;

import by.training.finalproject.model.Category;
import by.training.finalproject.repository.*;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDaoH2 implements CategoryDao {
    @Autowired
    private SessionFactory sessionFactory;
    
    public List<Category> getAll() {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Category", Category.class);
        return query.list();
    }
}
