package by.training.finalproject.controller;

import by.training.finalproject.model.Urgency;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/urgencies", produces = MediaType.APPLICATION_JSON_VALUE)
public class UrgencyController {
    @GetMapping
    public List<Urgency> getAll() {
        return Arrays.asList(Urgency.values());
    }
}
