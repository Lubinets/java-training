package by.training.finalproject.controller;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.dto.HistoryDTO;
import by.training.finalproject.service.HistoryService;
import java.lang.reflect.Type;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/history", produces = MediaType.APPLICATION_JSON_VALUE)
public class HistoryController {
    @Autowired
    private HistoryService service;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping(path = "/tickets/{id}")
    public ResponseEntity<?> getHistoryList(@PathVariable(value = "id") long ticketId)
            throws NotFoundException {
        Type listType = new TypeToken<List<HistoryDTO>>() {}.getType();
        return ResponseEntity.ok(modelMapper.map(service.getHistoryList(ticketId), listType));
    }
}
