package by.training.finalproject.model.dto;

import by.training.finalproject.model.Attachment;
import by.training.finalproject.model.Category;
import by.training.finalproject.model.Comment;
import by.training.finalproject.model.Urgency;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;


public class TicketCreateDTO {
    private Category category;
    
    private String name;
    
    private String description;
    
    private Urgency urgency;
    
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date desiredResolutionDate;
    
    
    private String comment;
    
    public TicketCreateDTO() {
        
    }

    /**
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = new Category();
        this.category.setId(Long.valueOf(category));
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the urgency
     */
    public Urgency getUrgency() {
        return urgency;
    }

    /**
     * @param urgency the urgency to set
     */
    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    /**
     * @return the desiredResolutionDate
     */
    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    /**
     * @param desiredResolutionDate the desiredResolutionDate to set
     */
    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
}
