package by.training.finalproject.service;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Feedback;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.TicketDao;
import java.security.Principal;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import javax.mail.MessagingException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FeedbackService {
    private final Logger logger = LogManager.getLogger(this.getClass());
    
    @Autowired
    private TicketDao ticketDao;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private EmailService emailService;
    
    @Transactional
    public Feedback getByTicketId(long id) throws NotFoundException {
        Optional<Ticket> ticketOpt = ticketDao.get(id);
        if(!ticketOpt.isPresent()) {
            throw new NotFoundException();
        }
        
        Feedback feedback = ticketOpt.get().getFeedback();
        if(Objects.isNull(feedback)) {
            throw new NotFoundException("No feedback");
        }
        return feedback;
    }
    
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void createFeedback(long ticketId, Principal principal, Feedback feedback)
            throws NotFoundException, NoPermissionException, MessagingException {
        Optional<Ticket> ticketOpt = ticketDao.get(ticketId);
        User user = userService.getUserByName(principal.getName());
        if(!ticketOpt.isPresent()) {
            throw new NotFoundException();
        }
        
        Ticket ticket = ticketOpt.get();
        if(!ticket.getTicketOwner().equals(user) || !ticket.getState().equals(State.DONE)) {
            throw new NoPermissionException();
        }
        
        if(Objects.nonNull(ticket.getFeedback())) {
            Feedback updatedFeedback = ticket.getFeedback();
            updatedFeedback.setRate(feedback.getRate());
            updatedFeedback.setText(feedback.getText());
            updatedFeedback.setDate(new Date());
            updatedFeedback.setUser(user);
        } else {
            feedback.setDate(new Date());
            feedback.setUser(user);
            ticket.setFeedback(feedback);
        }
        ticketDao.update(ticket);
        emailService.sendFeedbackProvided(ticket);
        logger.info("Feedback was provided for ticket " + ticketId);
    }
}
