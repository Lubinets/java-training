package by.training.finalproject.service;

import by.training.finalproject.model.User;
import by.training.finalproject.repository.UserDao;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class UserDetailsServiceH2 implements UserDetailsService {
    @Autowired
    private UserDao userDao;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOpt = userDao.getByName(username);
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.disabled(false);
            builder.password(user.getPassword());
            builder.authorities(user.getRole().toString());
        }  else {
            throw new UsernameNotFoundException("User not found.");
        }
        return builder.build();
    }
}
