package by.training.homework09;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Utility class with methods to sort array.
 */
public class Sorter {
    private Sorter() { }
    
    /**
     * Sorts array by its numbers sum of digits.
     * @param array array to sort
     */
    public static void sortArray(ArrayList<Integer> array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        Comparator<Integer> comp = (element1, element2) -> 
                Integer.compare(getNumberSum(element1), getNumberSum(element2));
        array.sort(comp);
    }
    
    /**
     * Gets sum of digits of the number.
     * @param number input number
     * @return returns sum of digits of the number
     */
    private static int getNumberSum(int number) {
        String stringNumber = Integer.toString(number);
        char[] charArray = stringNumber.toCharArray();
        int result = 0;
        for (char c : charArray) {
            result += Character.getNumericValue(c);
        }
        return result;
    }
}
