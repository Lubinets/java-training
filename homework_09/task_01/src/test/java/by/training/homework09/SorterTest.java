package by.training.homework09;

import by.training.homework09.Sorter;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.Assert;

/**
 * @author Admin
 */
public class SorterTest {
    ArrayList<Integer> actual;
    ArrayList<Integer> expected;
    
    public SorterTest() {
        expected = new ArrayList();
        expected.add(11);
        expected.add(21);
        expected.add(40);
        expected.add(5);
        expected.add(6);
        expected.add(54);
        
        actual = new ArrayList();
        actual.add(5);
        actual.add(6);
        actual.add(21);
        actual.add(11);
        actual.add(54);
        actual.add(40);
        
    }
    
    @Test
    public void testSortArray() {
        Sorter.sortArray(actual);
        Assert.assertEquals(expected, actual);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSortArrayWithNull() {
        Sorter.sortArray(null);
    }
}
