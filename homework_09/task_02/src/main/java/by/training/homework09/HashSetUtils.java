package by.training.homework09;

import java.util.HashSet;

/**
 * Utility class that contains mathematical operations on sets.
 */
public class HashSetUtils {
    private HashSetUtils() {}
    
    /**
     * Combines elements of two sets into one.
     * @param firstSet first input set
     * @param secondSet second input set
     * @return returns result set
     */
    public static HashSet<?> union(HashSet firstSet, HashSet secondSet) {
        if (firstSet == null || secondSet == null) {
            throw new IllegalArgumentException();
        }
        HashSet result = new HashSet();
        result.addAll(firstSet);
        result.addAll(secondSet);
        return result;
    }
    

    /**
     * Creates a new set with elements that are only present in both input sets.
     * @param firstSet first input set
     * @param secondSet second input set
     * @return returns result set
     */
    public static HashSet<?> intersection(HashSet firstSet, HashSet secondSet) {
        if (firstSet == null || secondSet == null) {
            throw new IllegalArgumentException();
        }
        HashSet result = new HashSet();
        result.addAll(firstSet);
        result.retainAll(secondSet);
        return result;
    }
    
    /**
     * Returns new set with elements from firsSet that are not present in 
     * secondSet.
     * @param firstSet first input set
     * @param secondSet second input set
     * @return returns result set
     */
    public static HashSet<?> minus(HashSet firstSet, HashSet secondSet) {
        if (firstSet == null || secondSet == null) {
            throw new IllegalArgumentException();
        }
        HashSet result = new HashSet();
        result.addAll(firstSet);
        result.removeAll(secondSet);
        return result;
    }
    
    /**
     * Returns new set with elements of firstSet that are not present in 
     * secondSet and elements of secondSet that are not present in firstSet.
     * @param firstSet first input set
     * @param secondSet second input set
     * @return returns result set
     */
    public static HashSet<?> difference(HashSet firstSet, HashSet secondSet) {
        if (firstSet == null || secondSet == null) {
            throw new IllegalArgumentException();
        }
        HashSet result = new HashSet();
        result.addAll(firstSet);
        for (Object item : secondSet) {
            if (result.contains(item)) {
                result.remove(item);
            } else {
                result.add(item);
            }
        }
        return result;
    }
}
