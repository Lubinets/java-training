/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.training.homework09;

import by.training.homework09.HashSetUtils;
import java.util.HashSet;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class HashSetUtilsTest {
    private HashSet expected;
    private HashSet firstSet;
    private HashSet secondSet;
    
    public HashSetUtilsTest() {
        firstSet = new HashSet<Integer>();
        secondSet = new HashSet<Integer>();
        firstSet.add(5);
        firstSet.add(6);
        firstSet.add(7);
        firstSet.add(15);
        secondSet.add(15);
        secondSet.add(7);
        secondSet.add(8);
        secondSet.add(9);
    }

    @Test
    public void testUnion() {
        expected = new HashSet<Integer>();
        expected.add(5);
        expected.add(15);
        expected.add(9);
        expected.add(6);
        expected.add(7);
        expected.add(8);
        HashSet actual = HashSetUtils.union(firstSet, secondSet);
        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testIntersection() {
        expected = new HashSet<Integer>();
        expected.add(15);
        expected.add(7);
        HashSet actual = HashSetUtils.intersection(firstSet, secondSet);
        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testMinus() {
        expected = new HashSet<Integer>();
        expected.add(5);
        expected.add(6);
        HashSet actual = HashSetUtils.minus(firstSet, secondSet);
        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testDifference() {
        expected = new HashSet<Integer>();
        expected.add(5);
        expected.add(6);
        expected.add(8);
        expected.add(9);
        HashSet actual = HashSetUtils.difference(firstSet, secondSet);
        Assert.assertEquals(expected, actual);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testUnionWithNull() {
        HashSetUtils.union(null, secondSet);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIntersectionWithNull() {
        HashSetUtils.intersection(null, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testMinusWithNull() {
        HashSetUtils.minus(firstSet, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDifferenceWithNull() {
        HashSetUtils.difference(null, secondSet);
    }  
}
