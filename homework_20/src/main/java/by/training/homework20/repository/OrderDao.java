package by.training.homework20.repository;

import by.training.homework20.model.Order;

/**
 * Dao to work with Orders table.
 */
public interface OrderDao {
    /**
     * Save order into database.
     * @param order order to save
     * @return returns id of the saved order
     */
    public int save(Order order);
    
    /**
     * Updates total price of the order.
     * @param id id of the order to update
     * @param totalPrice total price of the order
     */
    public void updateTotalPrice(int id, double totalPrice);
}
