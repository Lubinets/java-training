package by.training.homework20.repository;

import by.training.homework20.model.Product;
import by.training.homework20.model.ProductRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link ProductDao} for H2 database.
 */
@Repository
public class ProductDaoH2 implements ProductDao {
    private final String GET_ALL = "SELECT * FROM Product";
    private final String GET = "SELECT * FROM PRODUCT WHERE ID = ?";

    private JdbcTemplate connection;
    
    @Autowired
    public ProductDaoH2(JdbcTemplate template) {
        connection = template;
    }

    @Override
    public Optional get(int id) {
        return Optional.of(connection.queryForObject(GET, 
                new Object[] {id}, new ProductRowMapper()));
    }

    @Override
    public List<Product> getAll() {
        List<Product> resultList = connection.query(GET_ALL,
                new ProductRowMapper());
        return resultList;
    }
}
