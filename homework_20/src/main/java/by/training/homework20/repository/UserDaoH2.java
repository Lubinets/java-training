package by.training.homework20.repository;

import by.training.homework20.model.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Implementation of {@link UserDao} for H2 database.
 */
@Repository
public class UserDaoH2 implements UserDao {
    private final String GET_BY_NAME = "SELECT ID, login FROM User WHERE login = ?";
    private final String GET = "SELECT ID, login FROM User WHERE ID = ?";
    
    private JdbcTemplate connection;
    
    @Autowired
    public UserDaoH2(JdbcTemplate template) {
        connection = template;
    }
    
    @Override
    public Optional get(int id) {
        return Optional.of(connection.queryForObject(GET, 
                new Object[] {id}, new UserRowMapper()));
    }
    
    @Override
    public Optional getByName(String login) {
        return Optional.of(connection.queryForObject(GET_BY_NAME, 
                new Object[] {login}, new UserRowMapper()));
    }
}
