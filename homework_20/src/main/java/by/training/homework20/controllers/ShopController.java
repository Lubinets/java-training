package by.training.homework20.controllers;

import by.training.homework20.model.OrderGood;
import by.training.homework20.model.Product;
import by.training.homework20.repository.OrderGoodDao;
import by.training.homework20.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ShopController {
    @Autowired
    ProductDao productDao;
    
    @Autowired
    OrderGoodDao orderGoodDao;
    
    @GetMapping(value = "/shop")
    public String loadGoods(Model model, HttpServletRequest request, HttpSession session) {
        int orderId = (Integer) session.getAttribute("orderId");
        List<Product> selectedProducts = new ArrayList();
        List<OrderGood> orderGoods = orderGoodDao.getByOrderId(orderId);
        for (OrderGood good : orderGoods) {
            Optional<Product> productOpt = productDao.get(good.getGoodId());
            productOpt.ifPresent((product) -> {
                selectedProducts.add(product);
            });
        }
        request.setAttribute("goodsList", productDao.getAll());
        request.setAttribute("selectedItems", selectedProducts);
        return "shop";
    }
    
    @PostMapping(value = "/add-to-cart")
    public String addToCart(Model model, HttpServletRequest request, HttpSession session) {
        int productId = Integer.valueOf(request.getParameter("selectedItem"));
        int orderId = (Integer) request.getSession().getAttribute("orderId");
        OrderGood order = new OrderGood(0, orderId, productId);
        orderGoodDao.save(order);
        return "redirect:/shop";
    }
}
