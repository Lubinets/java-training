package by.training.homework20.controllers;

import by.training.homework20.model.Product;
import by.training.homework20.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CartController {
    @Autowired
    OrderService service;
    
    @GetMapping(value = "/cart")
    public String getUserCart(Model model, HttpServletRequest request) {
        int orderId = (Integer) request.getSession().getAttribute("orderId");
        List<Product> selectedProducts = service.getAllOrderedProductsById(orderId);
        if (!selectedProducts.isEmpty()) {
            request.setAttribute("cost", service.getTotalPrice());
            request.setAttribute("emptyCart", "false");
        } else {
            request.setAttribute("emptyCart", "true");
        }
        
        request.setAttribute("selectedItems", selectedProducts);
        return "cart";
    }
}
