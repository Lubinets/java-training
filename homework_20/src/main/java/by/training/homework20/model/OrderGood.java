package by.training.homework20.model;


public class OrderGood {
    private int id;
    private int orderId;
    private int goodId;
    
    public OrderGood(int id, int orderId, int goodId) {
        this.id = id;
        this.orderId = orderId;
        this.goodId = goodId;
    }

    public OrderGood() {
        
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the orderId
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the goodId
     */
    public int getGoodId() {
        return goodId;
    }

    /**
     * @param goodId the goodId to set
     */
    public void setGoodId(int goodId) {
        this.goodId = goodId;
    }
}
