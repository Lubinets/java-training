package by.training.homework11;

import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Admin
 */
public class SerializationTest {
    private final Folder expectedRoot;
    
    public SerializationTest() {
        FileStructureBuilder builder = new FileStructureBuilder("test\\testing\\test.exe");
        expectedRoot = new Folder("root");
        builder.build(expectedRoot);
    }
    
    @Test
    public void testSerialization() {
        expectedRoot.toFile("testSerialization.ser");
        Folder actualRoot = FileStructureBuilder.buildFromFile("testSerialization.ser");
        Assert.assertEquals(expectedRoot.print(""), actualRoot.print(""));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testBuildFromFileWithNull() {
        Folder actualRoot = FileStructureBuilder.buildFromFile(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testToFileWithNull() {
        expectedRoot.toFile(null);
    }
    
}
