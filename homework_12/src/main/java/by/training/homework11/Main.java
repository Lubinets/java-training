package by.training.homework11;

import static java.lang.System.in;
import java.util.Scanner;

public class Main {

    /**
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(in);
        String input;
        Folder root = new Folder("root");
        boolean menuCycle = true;
        while (menuCycle) {
            System.out.println("Enter command");
            input = scan.nextLine();
            switch (input) {
                case ("add") :
                    add(root);
                    break;
                case ("print") :
                    print(root);
                    break;
                case ("save") :
                    root.toFile("fileStructure.ser");
                    break;
                case ("load") :
                    root = FileStructureBuilder.buildFromFile(
                            "fileStructure.ser");
                    break;
                case ("exit") :
                    menuCycle = false;
                    break;
                default :
                    System.out.println("Invalid command");
                    break;
            }
        }
    }

    /**
     * Prints string representation of file structure to console.
     * @param root root folder of structure
     */
    private static void print(final Folder root) {
        System.out.println(root.print(""));
    }

    /**
     * Adds new path to file structure.
     * @param root root folder of structure
     */
    private static void add(final Folder root) {
        Scanner scan = new Scanner(in);
        System.out.println("Enter path");
        String input = scan.nextLine();
        FileStructureBuilder builder = new FileStructureBuilder(input);
        builder.build(root);
    }
}
