package by.training.homework11;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class responsible for building file structure from string path.
 */
public class FileStructureBuilder {
    private String path;

    public FileStructureBuilder(final String input) {
        path = input;
    }

    /**
     * Checks path for validity.
     * @param input path to check
     * @return returns the result of verification
     */
    private boolean verifyInput(final String input) {
        Pattern pattern = Pattern.compile("[^/:*?<>|+]+");
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    /**
     * Builds file structure from path.
     * @param root root folder to create structure
     */
    public void build(final Folder root) {
        if (!verifyInput(path)) {
            throw new IllegalArgumentException();
        }
        String[] pathTokens = path.split("\\\\");
        Folder tmp = root;
        Printable element;
        for (String s : pathTokens) {
            int i = tmp.getChildIndex(s);
            if (i == -1) {
                if (File.isFileName(s)) {
                    element = new File(s);
                    tmp.addChild(element);
                    break;
                } else {
                    element = new Folder(s);
                    tmp.addChild(element);
                    tmp = (Folder) element;
                }
            } else {
                if (!File.isFileName(s)) {
                    tmp = (Folder) tmp.getChild(i);
                }
            }
        }
    }
    
    /**
     * Gets file structure from file.
     * @param fileName name of the file to get saved structure
     * @return returns root folder with built structure or empty root folder 
     * if can't load structure from file
     */
    public static Folder buildFromFile(String fileName) {
        if (fileName == null) {
            throw new IllegalArgumentException();
        }
        Folder root;
        try (ObjectInputStream objIn = new ObjectInputStream(
                new FileInputStream(fileName))) {
            root = (Folder) objIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getLocalizedMessage());
            root = new Folder("root");
        }
        return root;
    }
}
