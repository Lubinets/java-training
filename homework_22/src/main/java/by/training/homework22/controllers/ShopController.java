package by.training.homework22.controllers;

import by.training.homework22.model.OrderGood;
import by.training.homework22.service.OrderService;
import by.training.homework22.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class ShopController {
    @Autowired
    ProductService productService;
    
    @Autowired
    OrderService orderService;

    
    @GetMapping(value = "/shop")
    public Model loadGoods(Model model, HttpSession session) {
        int orderId = (Integer) session.getAttribute("orderId");
        model.addAttribute("goodsList", productService.getProductList());
        model.addAttribute("selectedItems", productService.getSelectedProducts(orderId));
        return model;
    }
    
    @PostMapping(value = "/add-to-cart")
    public String addToCart(Model model, HttpServletRequest request, HttpSession session) {
        int productId = Integer.valueOf(request.getParameter("selectedItem"));
        int orderId = (Integer) session.getAttribute("orderId");
        OrderGood order = new OrderGood(0, orderId, productId);
        orderService.addToCart(order);
        return "redirect:/shop";
    }
}
