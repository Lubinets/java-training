package by.training.homework22.repository.impl;

import by.training.homework22.model.Order;
import by.training.homework22.repository.OrderDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Implementation of {@link OrderDao} for H2 database. 
 */
@Repository
public class OrderDaoH2 implements OrderDao {   
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public int save(Order order) {
        Transaction transaction = null;
        int orderId = -1;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(order);
            transaction.commit();
            orderId = order.getId();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return orderId;
    }

    @Override
    public void updateTotalPrice(int id, double totalPrice) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            Order order = (Order) session.load(Order.class, id);
            order.setTotalPrice(totalPrice);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            Logger.getLogger(OrderDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
