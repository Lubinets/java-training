package by.training.homework22.repository.impl;

import by.training.homework22.model.User;
import by.training.homework22.repository.UserDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Implementation of {@link UserDao} for H2 database.
 */
@Repository
public class UserDaoH2 implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Optional get(int id) {
        try (Session session = sessionFactory.openSession()) {
            return Optional.of(session.get(User.class, id));
        } catch (Exception ex) {
            Logger.getLogger(UserDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    @Override
    public Optional getByName(String login) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from User where name = :login", User.class);
            query.setParameter("login", login);
            return Optional.ofNullable(query.list().get(0));
        } catch (Exception ex) {
            Logger.getLogger(UserDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }
}
