package by.training.homework22.repository.impl;

import by.training.homework22.model.OrderGood;
import by.training.homework22.repository.OrderGoodDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Implementation of {@link OrderGoodDao} for H2 database.
 */
@Repository
public class OrderGoodDaoH2 implements OrderGoodDao {  
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void save(OrderGood orderGood) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(orderGood);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            Logger.getLogger(OrderGoodDaoH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<OrderGood> getByOrderId(int orderId) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from OrderGood where orderId = :orderId", OrderGood.class);
            query.setParameter("orderId", orderId);
            return query.list();
        } catch (Exception ex) {
            Logger.getLogger(OrderGoodDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }
}
