package by.training.homework22.repository;

import by.training.homework22.model.Product;

import java.util.List;
import java.util.Optional;

/**
 * Dao to work with Product table.
 */
public interface ProductDao {
    /**
     * Gets product by id from the database.
     * @param id id of the product
     * @return Optional of product
     */
    public Optional<Product> get(int id);
    
    /**
     * Gets list of all products in the database.
     * @return return list of products.
     */
    public List<Product> getAll();
}
