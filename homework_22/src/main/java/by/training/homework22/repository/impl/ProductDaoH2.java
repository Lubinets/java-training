package by.training.homework22.repository.impl;

import by.training.homework22.model.Product;
import by.training.homework22.repository.ProductDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Implementation of {@link ProductDao} for H2 database.
 */
@Repository
public class ProductDaoH2 implements ProductDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Optional get(int id) {
        try (Session session = sessionFactory.openSession()) {
            return Optional.ofNullable(session.get(Product.class, id));
        } catch (Exception ex) {
            Logger.getLogger(ProductDaoH2.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    @Override
    public List<Product> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Product", Product.class).list();
        } catch (Exception ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList();
        }
    }
}
