package by.training.homework22.service;

import by.training.homework22.model.Order;
import by.training.homework22.model.User;
import by.training.homework22.repository.OrderDao;
import by.training.homework22.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {
    @Autowired
    UserDao userDao;
    
    @Autowired
    OrderDao orderDao;
    
    /**
     * Gets user from database by his login.
     * @param name login of the user
     * @return returns optional of user
     */
    public Optional<User> getUserByName(String name) {
        return userDao.getByName(name);
    }
    
    /**
     * Creates new order for user.
     * @param userId id of the user
     * @return returns id of the createad order
     */
    public int createOrder(int userId) {
        Order order = new Order(0, userId, 0);
        return orderDao.save(order);
    }
}
