package by.training.homework22.service;

import by.training.homework22.model.OrderGood;
import by.training.homework22.model.Product;
import by.training.homework22.repository.OrderGoodDao;
import by.training.homework22.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ProductService {
    @Autowired
    ProductDao productDao;
    
    @Autowired
    OrderGoodDao orderGoodDao;
    
    public List<Product> getSelectedProducts(int orderId) {
        List<Product> selectedProducts = new ArrayList();
        List<OrderGood> orderGoods = orderGoodDao.getByOrderId(orderId);
        for (OrderGood good : orderGoods) {
            Optional<Product> productOpt = productDao.get(good.getGoodId());
            productOpt.ifPresent((product) -> {
                selectedProducts.add(product);
            });
        }
        return selectedProducts;
    }
    
    public List<Product> getProductList() {
        return productDao.getAll();
    }
}
