package by.training.homework22.service;

import by.training.homework22.model.OrderGood;
import by.training.homework22.model.Product;
import by.training.homework22.repository.OrderDao;
import by.training.homework22.repository.OrderGoodDao;
import by.training.homework22.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;
    
    @Autowired
    private OrderGoodDao orderGoodDao;
    
    @Autowired
    private ProductDao productDao;
    
    
    /**
     * Updates price of the order.
     * @param orderId id of the order
     * @return new price of the order
     */
    public double updateOrderPrice(int orderId) {
        double totalPrice = 0;
        List<OrderGood> orderGoods = orderGoodDao.getByOrderId(orderId);
        if (!orderGoods.isEmpty()) {
            for (OrderGood good : orderGoods) {
                Optional<Product> productOpt = productDao.get(good.getGoodId());
                if (productOpt.isPresent()) {
                    Product product = productOpt.get();
                    totalPrice += product.getProductPrice();
                }
            }
            orderDao.updateTotalPrice(orderId, totalPrice);
        }      
        return totalPrice;
    }
    
    /**
     * Adds product to cart.
     * @param orderGood 
     */
    public void addToCart(OrderGood orderGood) {
        orderGoodDao.save(orderGood);
    }
    
    /**
     * @param orderDao the orderDao to set
     */
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * @param orderGoodDao the orderGoodDao to set
     */
    public void setOrderGoodDao(OrderGoodDao orderGoodDao) {
        this.orderGoodDao = orderGoodDao;
    }

    /**
     * @param productDao the productDao to set
     */
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }
}
