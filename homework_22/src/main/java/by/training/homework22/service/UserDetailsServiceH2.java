package by.training.homework22.service;

import by.training.homework22.model.User;
import by.training.homework22.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceH2 implements UserDetailsService {
    @Autowired
    private UserDao userDao;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOpt = userDao.getByName(username);
        UserBuilder builder = null;
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            System.out.println(user.getName() + " " + user.getPassword());
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.disabled(false);
            builder.password(user.getPassword());
            builder.authorities("ROLE_USER");
        }  else {
            throw new UsernameNotFoundException("User not found.");
        }
        return builder.build();
    }    
}
